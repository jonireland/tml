<?php include("./includes/header.inc"); ?>     
    <div class="accommodation">
           <?php
         foreach($page->accommodation as $col) {

        $out = "<div class='media-object'>";
        $out .= "<h4><a target='_blank' href='$col->accommodation_web'>$col->accommodation_name</a>";
        if ($col->accommodation_type === 1) {
           $out .= " <small>- Hotel</small>";
        } else {
          $out .= " <small>- Bed &amp; Breakfast</small>";
        }
        $out .= "</h4>";
        $out .= "<p class='price'><strong>Prices from:</strong> $col->accommodation_price</p>";
        $out .= "<p><i class='fa fa-map-marker'></i> $col->accommodation_address</p>";
        $out .= "<span class='button'><i class='fa fa-phone-square'></i> $col->accommodation_tel</span>"; 
        $out .= "<a target='_blank' href='$col->accommodation_web' class='button secondary'><i class='fa fa-external-link'></i> Visit Website</a><a href='mailto:$col->accommodation_email' class='button secondary'><i class='fa fa-envelope-o'></i> Email</a>";
        if (strlen(trim($col->accommodation_special)) > 0) {
          $out .= "<div class='special'><p><strong>Special Offer:</strong> $col->accommodation_special</p></div>";
        }
        $out .= "</div>";
        echo $out;
}
          ?>
    </div>
        </main>
        <?php include("./includes/sidebar.inc"); ?>
      </section> 
