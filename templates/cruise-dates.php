<section class="main-content">
        <main><!--| Main Body |-->
          <h1>Booking Form</h1>
          <p>Please note that a 20% non-returnable deposit is required to secure your booking and the payment balance is due 12 weeks before your cruise start date.</p>
          <p>Please check the cruise information and complete the form below.</p>
<div class="callout secondary">
  <h5>Selected Cruise</h5>
  <?php  $price = number_format($page->cruise_price,2);?>
  <p><strong>Cruise : </strong><?php echo $page->parent->title; $vessel = $page->vessels->title;?></p>
  <p><strong>Vessel : </strong><?php echo $vessel;?></p>
  <p><strong>Start Date : </strong><?php echo $page->cruise_start;?></p>
  <p><strong>Number of Nights : </strong><?php echo $page->parent->number_of_nights;?></p>
  <p><strong>Price Per Person : </strong>&pound;<?php echo $price;?></p>

    <?php
$today = date("Y/m/d");
$datetime1 = date_create($page->cruise_start);
$datetime2 = date_create($today);
$interval = date_diff($datetime1, $datetime2);
$interval = $interval->format('%a');

if ($interval >= 84) {
  $paymentrequired = 20;
} else {
   $paymentrequired = 100;
}?>

<script>
     jQuery(function($) {

      $( document ).ready(function() {
    if($('#Inputfield_spaces').val()===''){
      $('.cart-box').hide();}
  });

        $('#Inputfield_spaces').change(function() {
    // Remove any previously set values
    $('#show_box, #total_box').empty();

    var spaces = $('#Inputfield_spaces').val();
    var price = parseFloat($('#price').val());
    var total = spaces * price;
    var new_num = total;
    var paytoday = (<?php echo $paymentrequired;?>/100) * total;
    var balance = total - paytoday;
    $('#show_box').append('<h6>' + price +' ' + spaces +'</h6>');
    $('#total_box').text(+ new_num);
    $('#number_guests').text(+ spaces);
    $('#amount_price').html('&pound;' +paytoday.toFixed(2));
    $('#total_price').html('&pound' + total.toFixed(2));
     $('#balance_price').html('&pound' + balance.toFixed(2));
    


if($('#Inputfield_spaces').val()===''){
      $('.cart-box').hide();
       $('.selectspaces').show();
    } else {
       $('.cart-box').show();
      $('.selectspaces').hide();
    }
});

     });

</script>


<?php


$cabinErr = $cabin = $fullnameErr = $emailErr = $address_1Err = $cityErr = $countryErr = $postcodeErr = $telErr = $mobileErr = "";
$fullname = $email = $address_1 = $address_2 = $city = $country = $postcode = $tel = $mobile= $spaces = $additional = $total = $payment = $amount = $book_single = $book_sing = "";
$status = $bookingid = $booking_single_space = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["fullname"])) {
    $fullnameErr = "required";
  } else {
    $fullname = test_input($_POST["fullname"]);
  }

  if (empty($_POST["email"])) {
    $emailErr = "required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }

  }
  if (empty($_POST["address_1"])) {
    $address_1Err = "required";
  } else {
    $address_1 = test_input($_POST["address_1"]);
  }

  if (empty($_POST["city"])) {
    $cityErr = "required";
  } else {
    $city = test_input($_POST["city"]);
  }

  if (empty($_POST["country"])) {
    $countryErr = "required";
  } else {
    $country = test_input($_POST["country"]);
  }

  if (empty($_POST["postcode"])) {
    $postcodeErr = "required";
  } else {
    $postcode = test_input($_POST["postcode"]);
  }

  if (empty($_POST["tel"])) {
    $telErr = "required";
  } else {
    $tel = test_input($_POST["tel"]);
  }

    if (empty($_POST["cabin"])) {
    $cabin = "required";
  } else {
    $cabin = test_input($_POST["cabin"]);
  }

  $address_2 = test_input($_POST["address_2"]);
  $mobile = test_input($_POST["mobile"]);
  $spaces = test_input($_POST["spaces"]);
  $additional = test_input($_POST["additional"]);
  $total = $spaces * $page->cruise_price;
  $payment = $paymentrequired/100 * $total;
  $singleprice = $page->cruise_price;

if ($fullnameErr !="" || $cabinErr !="" || $emailErr !="" || $address_1Err !="" || $cityErr != "" || $countryErr != "" || $postcodeErr != "" || $telErr != "") {
  echo "<div class='callout alert'>";
   echo "<p><strong style='color:red;'>You have missed some required fields please check the from and try again.</strong></p>";
   echo "</div>";
}else{

$single_spaces = $page->cruise_single;
if($spaces % 2 == 0) { //if spaces booked is even
} else { //if spaces booked is odd
       if($page->vessels->id == 1039 && $single_spaces >= 1) {
        $book_sing = 1;
        $booking_single_space = 1;

    }else if ($page->vessels->id == 1041 && $single_spaces == 1 || $page->vessels->id == 1042 && $single_spaces == 1 )        
    {
            $book_single = 1;
            $book_sing = 1;
            $booking_single_space = 2;
    }else if ($page->vessels->id == 1041 && $single_spaces == 2 || $page->vessels->id == 1042 && $single_spaces == 2 ){
            $book_sing = 1;
            $booking_single_space = 1;
}
}
$date = date_create();
$timestamp = date_format($date, 'YmdHis');
$cartid = 'TML-'.$timestamp.'-'.$page->id;

  $form = array(
  'parentcruiseid' => $page->parentID,
  'parentcruisename' => $page->parent->title,
  'nights' => $page->parent->number_of_nights,
  'id' => $page->id,
  'date' => $page->cruise_start,
  'vessel' => $vessel,
  'fullname' => $sanitizer->text($fullname),
  'email' => $sanitizer->email($email),
  'address_1' => $sanitizer->text($address_1),
  'address_2' => $sanitizer->text($address_2),
  'city' => $sanitizer->text($city),
  'country' => $sanitizer->text($country),
  'postcode' => $sanitizer->text($postcode),
  'cabin' => $sanitizer->text($cabin),
  'tel' => $sanitizer->text($tel),
  'mobile' => $sanitizer->text($mobile),
  'spaces' => $sanitizer->text($spaces),
  'additional' => $sanitizer->textarea($additional),
  'payment' => $sanitizer->text($payment),
  'total' => $sanitizer->text($total),
  'book_sing' => $book_sing,
  'book_single' => $book_single,
  'book_single_space' => $booking_single_space,
  'book_singleprice' => $singleprice,
  'single_spaces' => $single_spaces,
  'cartid'=> $cartid,
    ); 
  $session->order = $form;
  $paymentpage = $config->urls->root . "payment/";
  header('Location: '.$paymentpage);
}

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
  ?>
</div>
<p>If you would rather book by telephone please call us on +44 (0)1369 707 951.</p>

<form class='bookings' name="booking_from" method="POST" action="<?php echo $page->url; ?>" data-colspacing="0">
<div class="booking-box">
<div class="row">
    <div class="medium-12 columns">
      <h4>Customer Information</h4>
    </div>
  </div>
  <div class="Inputfields">
    <div class="row">
        <div class="medium-6 columns">
      <label for="fullname"><strong>Full Name</strong> <span class="error">* <?php echo $fullnameErr;?></span></label>
        <input name="fullname" type="text" maxlength="512" value="<?php echo $fullname;?>">
        
    </div>
    
    <div class="medium-6 columns">
        <label for="email"><strong>Email</strong> <span class="error">* <?php echo $emailErr;?></span></label>
        <input name="email" type="email" maxlength="512">
    </div>
  </div>
</div>

<div class="row">
    <div class="medium-6 columns">
      <label for="address_1"><strong>Address Line 1</strong>  <span class="error">* <?php echo $address_1Err;?></span></label>
      <input name="address_1" type="text" maxlength="500" value="<?php echo $address_1;?>">
    </div>
    
    <div class="medium-6 columns">
      <label for="address_2"><strong>Address Line 2</strong></label>
      <input name="address_2" type="text" maxlength="500" value="<?php echo $address_2;?>">
    </div>
</div>

<div class="row">
    <div class="medium-6 columns">
      <label for="city"><strong>City</strong>  <span class="error">* <?php echo $cityErr;?></span></label>
      <input name="city" type="text" maxlength="500" value="<?php echo $city;?>">
    </div>
    
    <div class="medium-6 columns">
      <label for="country"><strong>Country</strong> <span class="error">* <?php echo $countryErr;?></span></label>
      <input name="country" type="text" maxlength="500" value="<?php echo $country;?>">
    </div>
</div>
<div class="row">
    <div class="medium-6 columns">
      <label for="postcode"><strong>Postcode</strong> <span class="error">* <?php echo $postcodeErr;?></span></label>
      <input name="postcode" type="text" maxlength="50" value="<?php echo $postcode;?>">
    </div>

     <div class="medium-6 columns">
      <label for="tel"><strong>Tel No.</strong> <span class="error">* <?php echo $telErr;?></span></label>
      <input name="tel" type="text" maxlength="20" value="<?php echo $tel;?>">
    </div>
</div>
<div class="row">
    <div class="medium-6 columns">
      <label for="mobile"><strong>Mobile</strong></label>
      <input name="mobile" type="text" maxlength="20" value="<?php echo $mobile;?>">
    </div>
</div>


</div>
<div class="booking-box">    
<div class="row">
    <div class="medium-12 columns">
      <h4>Cruise Information</h4>
    </div>
  </div>

<div class="row">
    <div class="medium-6 columns">
          <label for="Inputfield_spaces">Number of guests</label>
        <select id="Inputfield_spaces" class=" required" name="spaces" required>
          <option value=''></option>
          <?php 
           $single_spaces = $page->cruise_single;
          if ($single_spaces == 0) {
            $range = range(2,$page->cruise_spaces,2);
           } else {
          $range = range(1,$page->cruise_spaces);}
          foreach ($range as $space) {echo "<option value='$space'>$space</option>";}?>
        </select>

      <label for="Inputfield_cabin">Cabin Type <span class="error">* <?php echo $cabinErr;?></span></label>
        <select id="Inputfield_cabin" class=" required" name="cabin" required>
          <option value=''></option>
          <option value='1'>Twin</option>
          <option value='2'>Double</option>
        </select>
    </div>    

    <div class="medium-6 columns">
      <label for="additional"><strong>Names of Additional Guests</strong></label>
      <textarea name="additional" maxlength="8192" rows="5"></textarea>
    </div>
</div>
</div>
<div class="cart-box">

<div class="row">
    <div class="medium-12 columns">
      <h4>Payment Information</h4>
    </div>
  </div>

<div class="row">
    <div class="medium-6 columns">
    <strong><span id="number_guests"></span> Guest(s)</strong> x &pound;<?php echo $price;?><br>
    <strong>Total Payment</strong> : <span id="total_price">£00.00</span><br>
    <strong>Balance Remaining</strong> : <span id="balance_price">£00.00</span><br>
    <div class="paytoday"><strong>Amount Payable today</strong> : <span id="amount_price">£00.00</span></div><br>
      
    </div> 
    <div class="medium-6 columns">
    <?php if ($paymentrequired == 100) {
  echo "<div class='callout warning'><p>Since this cruise departs within 12 weeks, the full balance is payable now.</p></div>";
}else{
  echo "<div class='callout success'><p>To secure your booking we will require a 20&#37; Deposit today.</p></div>";
}?>
    </div>    
</div> 

<div class="row">
    <div class="medium-12 columns">
    
    <input id="price" hidden class=" required InputfieldMaxWidth" readonly name="price" maxlength="512" value="<?php echo $page->cruise_price;?>">
    </div>

</div>

<div class="row">
    <div class="medium-6 columns">
      <button type="submit" name="booking_from_submit" value="Submit" class="button">Confirm Booking</button>
    </div>
    
    <div class="medium-6 columns">
<img class='float-right' src=http://www.worldpay.com/images/cardlogos/VISA.gif border=0 alt="Visa Credit and Debit payments supported by Worldpay">

<img class='float-right' src=http://www.worldpay.com/images/cardlogos/visa_electron.gif border=0 alt="Visa Electron payments supported by Worldpay">

<img class='float-right' src=http://www.worldpay.com/images/cardlogos/mastercard.gif border=0 alt="Mastercard payments supported by Worldpay">

<img class='float-right' src=http://www.worldpay.com/images/cardlogos/maestro.gif border=0 alt="Maestro payments supported by Worldpay">

<img class='float-right' src=http://www.worldpay.com/images/cardlogos/JCB.gif border=0 alt="JCB payments supported by Worldpay"></div>
    </div>
</div>
</div>
<div class="callout alert selectspaces">
  <h5>Please select spaces required before continuing</h5>
</div>

</form>

</main>
 <div class="main-sidebar"><!--| Sidebar Info |-->
   <?php
              // render widgets
              $widgets = $pages->get(1)->widget; 
              foreach($widgets as $widget) {
                echo $widget->render();
              }
         
              ?>  
</div>
</section>
<?php if($page->editable()) echo "<p><a href='$page->editURL'>Edit</a></p>";?>

