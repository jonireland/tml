
<?php
function sitemapListPage($page) {

        // create a list item & link to the given page, but don't close the <li> yet
        echo "<li><a href='{$page->url}'>{$page->title}</a> ";

        // check if the page has children, if so start a nested list
        if($page->numChildren) {
                // start a nested list
                echo "<ul>";

                // loop through the children, recursively calling this function for each
                foreach($page->children as $child) sitemapListPage($child);

                // close the nested list
                echo "</ul>";
        }

        // close the list item
        echo "</li>";
}

// include site header markup
include("./head.inc");

// start the sitemap unordered list
echo "<ul class='sitemap'>";

// get the homepage and start the sitemap
sitemapListPage($pages->get("/"));

// close the unordered list
echo "</ul>";?>  
