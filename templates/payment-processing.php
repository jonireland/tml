<!DOCTYPE html>
<html lang="en">
  <head>
    <?php $site = "https://themajesticline.co.uk";?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
            <?php
          echo "<link rel='stylesheet'href='{$config->urls->templates}css/app.css'>";
          echo '<link rel="stylesheet" href="/i/<wpdisplay item=instId>/app.css">';
        ?>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="<?php echo $site.$config->urls->templates?>bower_components/jquery/dist/jquery.js"></script>
    <script src="https://use.typekit.net/qcu2yhz.js"></script>
    <?php //if(isset($_POST['cartId']) && $_POST['transStatus'] == "Y") {
      //echo '<meta http-equiv="refresh" content="3;url=http://www.google.com/" />';
    //}?>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
  </head>
  <body>
  <?php if($page->editable()) echo "<div class='admin'><p>Clearbox Designs Edit Mode - <a href='$page->editURL'>Edit</a></p></div>"; ?>
  <div class="main-header hide-for-small">
  <div class="row">
      
      <div class="small-12 medium-5 columns">

        <a href="<?php echo $site.$config->urls->root?>"><img class="logo" src="/i/<wpdisplay item=instId>/majesticline-logo-new.png"></a>
      </div>
        
      <div class="medium-7 columns show-for-medium">
        <div class="row">
          <div class="large-12 columns">
            
                
                  <a href="<?php echo $site.$config->urls->root?>contact/" class="secondary button float-right"><i class="fa fa-envelope"></i></a> 
                  <div class="secondary button float-right"><i class="fa fa-phone"></i> +44(0) 1369 707 951</div>
                
                  <a class="icons float-right" href="https://www.instagram.com/themajesticline/" target="_blank"><span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                </span></a>
                <a class="icons float-right" href="https://www.facebook.com/TheMajesticLine/" target="_blank"><span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span></a>
                <a class="icons float-right" href="https://twitter.com/TheMajesticLine" target="_blank"><span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span></a>
              
          </div>

          <div class="large-12 columns">
            <a class="cruise-cal-button float-right" href="<?php echo $site.$config->urls->root?>calendar/"><span>View Our</span> Cruise Calendar <i class="fa fa-calendar"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="top-bar" id="main-menu">
<div class="row">
  <div class="medium-12 columns">
  <div class="top-bar-left">
    <ul class="menu-nav small">
      <li><a href="http://themajesticline.co.uk">Home</a></li>
    </ul>
  </div>
</div></div>
</div>
         <div class="row">
        <div class="medium-12 columns">
<?php
if (isset($_POST['cartId']) && $_POST['transStatus'] == 'Y')
{

  $cart = $_POST['cartId'];
  $worldpay_booking = $pages->find("wp_cartid=$cart");
  foreach ($worldpay_booking as $worldpay_booked) {
  $payment_status = isset($_POST['transStatus']);

if ($payment_status == 'Y') {
    $p = new Page(); // create new page object
$p->template = 'booking'; // set template
$p->parent = wire('pages')->get('/bookings/'); // set the parent 
$p->name = $worldpay_booked->name; // give it a name used in the url for the page
$p->title = $worldpay_booked->title;
$p->booking_name = $worldpay_booked->booking_name;
$p->booking_email = $worldpay_booked->booking_email;
$p->booking_address = $worldpay_booked->booking_address;
$p->booking_address1 = $worldpay_booked->booking_address1;
$p->booking_city = $worldpay_booked->booking_city;
$p->booking_country = $worldpay_booked->booking_country;
$p->booking_postcode = $worldpay_booked->booking_postcode;
$p->booking_tel = $worldpay_booked->booking_tel;
$p->booking_cabin = $worldpay_booked->booking_cabin;
$p->booking_mobile = $worldpay_booked->booking_mobile;
$p->booking_spaces = $worldpay_booked->booking_spaces;
$p->booking_title = $worldpay_booked->booking_title;
$p->booking_start = $worldpay_booked->booking_start;
$p->booking_cruiseid = $worldpay_booked->booking_cruiseid;
$p->booking_status = 2;
$p->booking_additional = $worldpay_booked->booking_additional;
$p->booking_price = $worldpay_booked->booking_price;
$p->booking_total = $worldpay_booked->booking_total;
$p->booking_amountpaid = $worldpay_booked->booking_amountpaid;
$p->booking_single = $worldpay_booked->booking_single;
$p->booking_vessel = $worldpay_booked->booking_vessel;
$p->wp_cartid = $cart;
$p->save();

$cruise = $pages->get($worldpay_booked->booking_cruiseid);
$cruise->setOutputFormatting(false);
$currentspaces = $cruise->get("cruise_spaces");
$spaces = $worldpay_booked->booking_spaces;
$book_single = $worldpay_booked->wp_book_single;
$single_spaces = $worldpay_booked->wp_single_spaces;
$book_sing = $worldpay_booked->wp_book_sing;
$newspaces =  $currentspaces-$spaces-$book_single;
$newsinglespaces =  $single_spaces-$book_sing;
$cruise->set("cruise_single", $newsinglespaces); 
$cruise->save("cruise_single");
$cruise->set("cruise_spaces", $newspaces); 
$cruise->save("cruise_spaces");

$unformated = $cruise->getUnformatted("cruise_start");
$add_date = date("Y-m-d H:m:s", $unformated); 
$expiry_date = new DateTime($add_date);
$expiry_date ->modify("-84 days");
$depositdate = $expiry_date ->format("j F Y");

if ($worldpay_booked->booking_total == $worldpay_booked->booking_amountpaid) {
  $deposit= "";
}else {
  $deposit= "<tr><td><strong>Final Payment:</strong> </td><td>{$depositdate}</td></tr>";
}

$mail = wireMail();
$mail->to("info@themajesticline.co.uk")->from("noreply@themajesticline.co.uk"); // all calls can be chained
$mail->subject("Cruise Booking via WorldPay - {$cart}"); 
$mail->body("New Booking");
$mail->bodyHTML("
  <html><body style='background:#163072;'>
  
  <table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>
        <td align='center'>
  <table rules='all' style='border-color: #163072; background:#fff;' cellpadding='10'>
  <tr><td colspan='2' align='center'><img src='https://themajesticline.co.uk/images/tmllogo.png' alt='The Majestic Logo' />
  <h3 style='color:#163072;'>New Cruise Booking</h3></td></tr>
  <tr><td><strong>Name:</strong> </td><td>{$worldpay_booked->booking_name}</td></tr>
  <tr><td><strong>Email:</strong> </td><td>{$worldpay_booked->booking_email}</td></tr>
  <tr><td><strong>Tel:</strong> </td><td>{$worldpay_booked->booking_tel}</td></tr>
  <tr><td><strong>Mobile:</strong> </td><td>{$worldpay_booked->booking_mobile}</td></tr>
  <tr><td><strong>Address :</strong> </td><td>{$worldpay_booked->booking_address}, {$worldpay_booked->booking_address1}, {$worldpay_booked->booking_city}, {$worldpay_booked->booking_postcode}</td></tr>
  <tr style='background: #b3a16e; color:#fff;'><td colspan='2' align='center'><strong>Cruise Info</strong> </td></tr>
  <tr><td><strong>Cruise:</strong> </td><td>{$worldpay_booked->booking_title}</td></tr>
  <tr><td><strong>Start Date:</strong> </td><td>{$worldpay_booked->booking_start}</td></tr>
  <tr><td><strong>Vessel:</strong> </td><td>{$worldpay_booked->booking_vessel}</td></tr>
  <tr><td><strong>Spaces booked:</strong> </td><td>{$worldpay_booked->booking_spaces}</td></tr>
  <tr><td><strong>Cabin Type:</strong> </td><td>{$worldpay_booked->booking_cabin->title}</td></tr>
  <tr><td><strong>Additional Guests:</strong> </td><td>{$worldpay_booked->booking_additional}</td></tr>
  <tr><td><strong>Cruise Price:</strong> </td><td>{$worldpay_booked->booking_price}</td></tr>
  <tr><td><strong>Total Price:</strong> </td><td>{$worldpay_booked->booking_total}</td></tr>
  <tr><td><strong>Amount Paid:</strong> </td><td>{$worldpay_booked->booking_amountpaid}</td></tr>
  {$deposit}
  <tr><td><strong>Singles Applied:</strong> </td><td>{$worldpay_booked->booking_single}</td></tr>
  </table>        </td>
    </tr>
</table></body></html>"); 
$mail->send(); 

  ?><br><br>
  <div class='order-summary'>
  <h1>Payment Successful</h1>
  <p><strong>Thank you for booking with The Majestic Line.</strong></p>
  <p>Your payment has been taken and you will receive a confirmation email shortly</p>
  <p>Many Thanks,</p>
  <p>The Majestic Line Team</p>
  <h2>Order Infomation</h2>
  <div id="worldpay-banner"><WPDISPLAY ITEM=banner></div>
  <a class="button" href="https://themajesticline.co.uk">Return to The Majestic Line</a>
  </div>
  <?php
}}
}else if ($_POST['transStatus'] == 'C'){
    ?>
     <h1 class="title">Booking Cancelled</h1>
    <p>You have cancelled your booking. No payment has been taking at this time. please phone us on +44 01369 707951 or <a href="mailto:info@themajesticline.co.uk?Subject=Booking Failed">email us</a> if you would like any help.</p>
    <a class="button" href="https://themajesticline.co.uk">Return to Homepage</a><?php
}else{
  ?>
    <h1 class="title">Payment Failed</h1>
    <p>Sorry your payment has failed (We couldn't find the booking you were trying to pay for in our system),
    please phone us on +44 01369 707951 or <a href="mailto:info@themajesticline.co.uk?Subject=Booking Failed">email us</a> to place your booking.</p>
    <a class="button" href="http://themajesticline.co.uk">Return to Homepage</a>
  <?php
}
?>
</div>
</div>
