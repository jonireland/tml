<section class="main-content">
        <main><!--| Main Body |-->

  <?php 
require_once('stripe/init.php');
$out = "";
$cruise_id = $session->order['id'];
if (isset($cruise_id)){


// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey("sk_test_wTzOiRcQ1LU45MOquSyae2S5");

// Get the credit card details submitted by the form
$token = $_POST['stripeToken'];

// Create the charge on Stripe's servers - this will charge the user's card
try {
  $charge = \Stripe\Charge::create(array(
    "amount" => $_POST['amount'],
    "currency" => "gbp",
    "source" => $token,
    "description" => $_POST['name'],
"receipt_email" => "jon@clearboxdesigns.co.uk",
"metadata" => array("cruise" => "this is the cruise", "total cost" => "40000"),
    ));
   
} catch(\Stripe\Error\Card $e) {
  // Since it's a decline, \Stripe\Error\Card will be caught
  $body = $e->getJsonBody();
  $err  = $body['error'];

  print('Status is:' . $e->getHttpStatus() . "\n");
  print('Type is:' . $err['type'] . "\n");
  print('Code is:' . $err['code'] . "\n");
  // param is '' in this case
  print('Message is:' . $err['message'] . "\n");
} catch (\Stripe\Error\RateLimit $e) {
  // Too many requests made to the API too quickly
} catch (\Stripe\Error\InvalidRequest $e) {
  // Invalid parameters were supplied to Stripe's API
} catch (\Stripe\Error\Authentication $e) {
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)
} catch (\Stripe\Error\ApiConnection $e) {
  // Network communication with Stripe failed
} catch (\Stripe\Error\Base $e) {
  // Display a very generic error to the user, and maybe send
  // yourself an email
} catch (Exception $e) {
  // Something else happened, completely unrelated to Stripe
}

$payment = $charge->paid;
if ($payment == 1) {

 $p = new Page(); // create new page object
$p->template = 'booking'; // set template
$p->parent = wire('pages')->get('/site-manager/bookings/'); // set the parent 
$p->name = strtotime ("now");; // give it a name used in the url for the page
$p->title = $session->order['parentcruisename'] . " " . $session->order['fullname'];
$p->booking_name = $session->order['fullname'];
$p->booking_email = $session->order['email'];
$p->booking_address = $session->order['address_1'];
$p->booking_address1 = $session->order['address_2'];
$p->booking_city = $session->order['city'];
$p->booking_country = $session->order['country'];
$p->booking_postcode = $session->order['postcode'];
$p->booking_tel = $session->order['tel'];
$p->booking_spaces = $session->order['spaces'];
$p->booking_title = $session->order['parentcruisename'];
$p->booking_start = $session->order['date'];
$p->booking_cruiseid = $session->order['id'];
$p->booking_additional = $session->order['additional'];
$p->booking_price = $session->order['book_singleprice'];
$p->booking_total = $session->order['total'];
$p->booking_amountpaid = $session->order['payment'];
$p->booking_single = $session->order['book_single_space'];
$p->booking_vessel = $session->order['vessel'];

// added by Ryan: save page in preparation for adding files (#1)
$p->save();




$cruise = $pages->get($cruise_id);
$cruise->setOutputFormatting(false);
$currentspaces = $cruise->get("cruise_spaces");

$spaces = $session->order['spaces'];
$book_single = $session->order['book_single'];
$single_spaces = $session->order['single_spaces'];
$book_sing = $session->order['book_sing'];


$newspaces =  $currentspaces-$spaces-$book_single;
$newsinglespaces =  $single_spaces-$book_sing;
$cruise->set("cruise_single", $newsinglespaces); 
$cruise->save("cruise_single");
$cruise->set("cruise_spaces", $newspaces); 
$cruise->save("cruise_spaces");

$status = 1;
$title = $session->order['parentcruisename'];
$spaces = $session->order['spaces'];
$pricep = number_format($session->order['book_singleprice'],2,'.', '');
$totalp = number_format($session->order['total'],2,'.', '');
$amountp = number_format($session->order['payment'],2,'.', '');
$amount = number_format((float)$session->order['payment'], 2, '', '');
$out .= "<div class='order-summary'>";
  $out .= "<h1>Booking Complete</h1>";
  $out .= "<p>Your booking has now been completed you will receive an email confirmation soon. You will be contacted by one of our shore team soon.</p>";
  $out .= "<strong>Cruise : </strong>$title</br>";
  $out .= "<strong>Spaces : </strong>$spaces x &pound;$pricep per person </br>";
  $out .= "<strong>Total : </strong>&pound;$totalp</br>";
   $out .= "<p class='amount'><strong>Amount to Paid : </strong>&pound;$amountp</p>";
  $out .= "</div>";

echo $out;
$session->remove("order");
}else{
echo "<div class='callout alert'>";
   echo "<h2>Payment Failed</h2>";
   echo "<p>Your payment has failed and we have been unable to book your cruise. Please try again or contact a memeber of our team on +44(0) 1369 707 951.</p>";
   echo "<a class='expanded button secondary' href='{$config->urls->root}cruises/'>Back to our Cruises</a></div>";
}
}else{
   echo "<div class='callout warning'>";
   echo "<h2>Please Select a Cruise</h2>";
   echo "<p>Please return to our cruises page and select a cruise.</p>";
   echo "<a class='expanded button secondary' href='{$config->urls->root}cruises/'>Back to our Cruises</a></div>";
  }
?>
</main>
 <div class="main-sidebar"><!--| Sidebar Info |-->
   <?php
              // render widgets
              $widgets = $pages->get(1)->widget; 
              foreach($widgets as $widget) {
                echo $widget->render();
              }
              ?>  
</div>
</section>
