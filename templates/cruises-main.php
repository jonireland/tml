   <section class="main-content">
        <main><!--| Main Body |-->
          <h1 class="title"><?php echo $page->title;?></h1>
          <?php echo $page->body;?>
        </main>
    
        <div class="main-sidebar"><!--| Sidebar Info |-->
            <ul class="menu vertical child-sub">
                    <?php
                    $children = $page->children->not("template=cruises");
                    foreach ($children as $child) {
                         if($child->id == $page->id) {
               echo "<li class='active'>";
           } else {
               echo "<li>";
           }
                        echo "<a href=\"{$child->url}\">{$child->title}</a></li>";}
                    ?>
                    </ul>
             <?php
              // render widgets
              $widgets = $page->widget; 
              foreach($widgets as $widget) {
                echo $widget->render();
              } 
              ?>    
        </div>
      </section> 
<div class="row medium-up-3 cruise-list" data-equalizer>
        <?php
            $children = $page->children("template=cruises, sort=sort")->not("exclude_from_cruise_list=1");
            
            foreach($children as $child) {

                echo "<div class='columns'>";
                echo "<a href='$child->url'><div class='bg' data-equalizer-watch>\r\n";
                if($child->single_image) 
                $sized = $child->single_image->size(530, 354);
                echo "<div class='image-container'><img class='img' src='{$sized->url}'></div>";
                echo "<p>{$child->title} {$child->number_of_nights}-night</p>";
                echo "</div></a>\r\n";
                echo "</div>";
                $sized = "0";
            }
            ?>
           <div class='columns'>
                <a href='<?php echo $config->urls->root?>private-charter'><div class='bg' data-equalizer-watch>
                <div class='image-container'><img class='img' src='<?php echo $config->urls->root?>site/templates/img/duart-bay.530x354.jpg'></div>
                <p>Private Cruise Charter</p>
               </div></a>
               </div>
               
</div>

