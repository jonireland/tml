

<div class="jump-to-cruise">
  <div class="row small-up-1 medium-up-2 large-up-3">
  <h2>Jump to Cruise</h2>

<?php

$out = '';

$cruise = $pages->find("template=cruises, sort=title");

foreach($cruise as $cruises) {
     if ($cruises->numChildren >= 1) {
    echo "<div class='column'><a href='#cruise{$cruises->id}'>{$cruises->title}</div>";}}
?>
</div></div>
<?php
foreach($cruise as $cruises) {
     if ($cruises->numChildren >= 1) {
    $out .= "<div class='add-booking'>";
    $out .= "<h2 id='cruise{$cruises->id}'>{$cruises->title} : <span>{$cruises->number_of_nights} Nights</span> - {$cruises->numChildren}</h2>";

    $cruisedate = $cruises->children("sort=cruise_start");
    $out .= "<table class='AdminDataTable AdminDataList AdminDataTableResponsive'>"; 
    $out .= "<thead><tr><th width='15%'>Cruise Start Date</th>";
    $out .= "<th width='15%'>Spaces Left</th>";
    $out .= "<th width='15%'>Price</th>";
    $out .= "<th width='15%'>Vessels</th>";
    $out .= "<th width='15%'>Nights</th>";
    $out .= "<th width='15%'>New Booking</th></tr></thead><tbody>";
            foreach($cruisedate as $dater) {
                   if ($dater->cruise_spaces >= 1) {
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
$form = array(
  'cruise_id' =>  $_POST["cruiseid"],
  ); 
  $session->order = $form;
  $addbooking = $config->urls->root . "site-manager/add-booking/";
  header('Location: '.$addbooking);
}

                    $out .= "<tr><td>{$dater->cruise_start}</td>";
                    $out .= "<td>{$dater->cruise_spaces}</td>";
                    $out .= "<td>&pound;{$dater->cruise_price}</td>";
                    $out .= "<td>{$dater->vessels->title}</td>";
                    $out .= "<td>{$cruises->number_of_nights}</td>";                  
                    $out .= "<td>";
                    $out .= "<form name='booking_from' method='POST' action='$page->url' data-colspacing='0'>";
                    $out .= "<input hidden readonly name='cruiseid' maxlength='512' value='$dater->id'>";
                    $out .= "<button class='small' type='submit' name='booking_from_submit' value='Submit' class='button success'>New Booking</button></form>";
                }
}
        $out .= "</tbody></table></div><p class='top'><a href='#'><i class='fa fa-chevron-up'></i> Return to Top</a></p>";
    }
}


echo $out;

?>


