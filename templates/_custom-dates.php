<?php 
$cruise_id = $session->order['cruise_id'];
if (isset($cruise_id)){

  }else{
   echo "<h2>Sorry you did'nt select a cruise.</h2>";
   exit;
  }

$cruise = $pages->get($cruise_id);
$cruise_title = $cruise->parent->title . " - " . $cruise->parent->number_of_nights . " Nights";
?>
<div class='add-booking'>
<h2>Cruise Information</h2>
<strong>Cruise Title :</strong> <?php echo $cruise_title; ?><br>
<strong>Cruise Start Date :</strong> <?php echo $cruise->cruise_start;?><br>
<strong>Cruise Price :</strong> &pound;<?php echo $cruise->cruise_price;?> per person<br>
<strong>Cruise Spaces :</strong> <?php echo $cruise->cruise_spaces;?><br>
<strong>Cruise ID :</strong> <?php echo $cruise_id;?><br>





</div>


    <?php
$today = date("Y/m/d");
$datetime1 = date_create($cruise->cruise_start);
$datetime2 = date_create($today);
$interval = date_diff($datetime1, $datetime2);
$interval = $interval->format('%a');

if ($interval >= 85) {
  $paymentrequired = 20;
} else {
   $paymentrequired = 100;
}?>

<script>
     jQuery(function($) {

      $( document ).ready(function() {
    if($('#Inputfield_spaces').val()===''){
      $('.cart-box').hide();}
  });
      	$('#checked').click(function(){
    $.ajax( '/change_complete_status.php?pageid=<?php echo $page->id; ?>');
});

        $('#Inputfield_spaces').change(function() {
    // Remove any previously set values
    $('#show_box, #total_box').empty();

    var spaces = $('#Inputfield_spaces').val();
    var price = parseFloat($('#price').val());
    var total = spaces * price;
    var new_num = total;
    var paytoday = (<?php echo $paymentrequired;?>/100) * total;
    var balance = total - paytoday;
    $('#show_box').append('<h6>' + price +' ' + spaces +'</h6>');
    $('#total_box').text(+ new_num);
    $('#number_guests').text(+ spaces);
    $('#amount_price').html('&pound;' +paytoday.toFixed(2));
    $('#total_price').html('&pound' + total.toFixed(2));
     $('#balance_price').html('&pound' + balance.toFixed(2));
    


if($('#Inputfield_spaces').val()===''){
      $('.cart-box').hide();
       $('.selectspaces').show();
    } else {
       $('.cart-box').show();
      $('.selectspaces').hide();
    }
});

     });

</script>


<?php

$fullnameErr = $emailErr = $address_1Err = $cityErr = $countryErr = $postcodeErr = $telErr = "";
$fullname = $email = $address_1 = $address_2 = $city = $country = $postcode = $tel = $spaces = $additional = $total = $payment = $amount = $book_single = $book_sing = "";
$status = $bookingid = $booking_single_space = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["fullname"])) {
    $fullnameErr = "required";
  } else {
    $fullname = test_input($_POST["fullname"]);
  }

    $email = test_input($_POST["email"]);

    $address_1 = test_input($_POST["address_1"]);

    $city = test_input($_POST["city"]);

    $country = test_input($_POST["country"]);

    $postcode = test_input($_POST["postcode"]);

    $tel = test_input($_POST["tel"]);
  

  $address_2 = test_input($_POST["address_2"]);
  $spaces = test_input($_POST["spaces"]);
  $additional = test_input($_POST["additional"]);
  $amount = test_input($_POST["amount"]);
  $total = $spaces * $cruise->cruise_price;
  $payment = $paymentrequired/100 * $total;


if ($fullnameErr !="" || $emailErr !="" || $address_1Err !="" || $cityErr != "" || $countryErr != "" || $postcodeErr != "" || $telErr != "") {
  echo "<div class='booking-box error'>Field(s) Empty Please Update</div>";

}else{

$single_spaces = $cruise->cruise_single;
if($spaces % 2 == 0) {//if spaces booked is even

} else { //if spaces booked is odd

   if($cruise->vessels->id == 1039 && $single_spaces >= 1) {
        $book_sing = 1;
        $booking_single_space = 1;

    }else if ($cruise->vessels->id == 1041 && $single_spaces == 1 || $cruise->vessels->id == 1042 && $single_spaces == 1 )        
    {
            $book_single = 1;
            $book_sing = 1;
            $booking_single_space = 2;
    }else if ($cruise->vessels->id == 1041 && $single_spaces == 2 || $cruise->vessels->id == 1042 && $single_spaces == 2 ){
            $book_sing = 1;
            $booking_single_space = 1;
}
}

  $p = new Page(); // create new page object
$p->template = 'booking'; // set template
$p->parent = wire('pages')->get('/site-manager/bookings/'); // set the parent 
$p->name = strtotime ("now"); // give it a name used in the url for the page
$p->title = $cruise_title . " ( " . $fullname. " ) ";
$p->booking_name = $fullname; // set page title (not neccessary but recommended)
$p->booking_email = $email;
$p->booking_address = $address_1;
$p->booking_address1 = $address_2;
$p->booking_city = $city;
$p->booking_country = $country;
$p->booking_postcode = $postcode;
$p->booking_tel = $tel;
$p->booking_spaces = $spaces;
$p->booking_title = $cruise_title;
$p->booking_start = $cruise->cruise_start;
$p->booking_cruiseid = $cruise_id;
$p->booking_additional = $additional;
$p->booking_price = $cruise->cruise_price;
$p->booking_total = $total;
$p->booking_amountpaid = $amount;
$p->booking_single = $booking_single_space;
$p->booking_vessel = $cruise->vessels->title;
// added by Ryan: save page in preparation for adding files (#1)
$p->save();
$bookingid = $p->id;
$cruise->setOutputFormatting(false);
$currentspaces = $cruise->get("cruise_spaces");

$newsinglespaces =  $single_spaces-$book_sing;
$cruise->set("cruise_single", $newsinglespaces); 
$cruise->save("cruise_single");

$newspaces =  $currentspaces-$spaces-$book_single;
$cruise->set("cruise_spaces", $newspaces); 
$cruise->save("cruise_spaces");

$status = 1;


// populate fields
}

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

if ($status == 1) {
echo "<div class='booking-box'>";
echo "<h2>Booking Added</h2>";
echo "<strong>Fullname : </strong>$fullname<br>";
echo "<strong>Address : </strong>$address_1 , $address_2 , $city , $country , $postcode<br>";
echo "<strong>Telephone :</strong>$tel<br>";
echo "<strong>Email :</strong>$email<br>";
echo "<strong>Spaces Required :</strong>$spaces<br>";
echo "<p><strong>Booking Ref :</strong>$bookingid<br></p>";
echo "</div>";
echo "<div class='callout success'><p>Booking Added.</p><BOOKING_div>";
$session->remove('order');

}else{


  $out = "<form name='booking_from' method='POST' action='$page->url' data-colspacing='0'>";
	$out .= "<div class='booking-box'>";
		$out .= "<h2>Customer Information</h2>";
			$out .= "<div class='Inputfields'>";
$out .= "<div class='row'>";
$out .= "<div class='medium-6 columns'>";
$out .= "<label for='fullname'><strong>Full Name</strong> <span class='error'>* $fullnameErr</span></label>";
$out .= "<input name='fullname' type='text' maxlength='512' value='$fullname'></div>";
        
    
    
$out .= "<div class='medium-6 columns'>";
$out .= "<label for='email'><strong>Email</strong> <span class='error'>* $emailErr</span></label>";
$out .= "<input name='email' type='email' maxlength='512'>";
$out .= "</div></div></div>";

$out .= "<div class='row'>";
    $out .= "<div class='medium-6 columns'>";
      $out .= "<label for='address_1'><strong>Address Line 1</strong>  <span class='error'>* $address_1Err</span></label>";
      $out .= "<input name='address_1' type='text' maxlength='500' value='$address_1'>";
    $out .= "</div>";
    
    $out .= "<div class='medium-6 columns'>";
      $out .= "<label for='address_2'><strong>Address Line 2</strong></label>";
      $out .= "<input name='address_2' type='text' maxlength='500' value='$address_2'>";
    $out .= "</div>";
$out .= "</div>";

$out .= "<div class='row'>";
    $out .= "<div class='medium-6 columns'>";
      $out .= "<label for='city'><strong>City</strong>  <span class='error'>* $cityErr</span></label>";
      $out .= "<input name='city' type='text' maxlength='500' value='$city'>";
    $out .= "</div>";
    
    $out .= "<div class='medium-6 columns'>";
      $out .= "<label for='country'><strong>Country</strong> <span class='error'>* $countryErr</span></label>";
      $out .= "<input name='country' type='text' maxlength='500' value='$country'>";
    $out .= "</div>";
$out .= "</div>";
$out .= "<div class='row'>";
    $out .= "<div class='medium-6 columns'>";
      $out .= "<label for='postcode'><strong>Postcode</strong> <span class='error'>* $postcodeErr</span></label>";
      $out .= "<input name='postcode' type='text' maxlength='50' value='$postcode'>";
    $out .= "</div>";

     $out .= "<div class='medium-6 columns'>";
      $out .= "<label for='tel'><strong>Tel No.</strong> <span class='error'>* $telErr</span></label>";
      $out .= "<input name='tel' type='text' maxlength='20' value='$tel'>";
    $out .= "</div>";
$out .= "</div></div>";
$out .= "<div class='booking-box'>";

      $out .= "<h2>Cruise Spaces</h2>";


$out .= "<div class='row'>";
    $out .= "<div class='medium-6 columns'>";
          $out .= "<label for='Inputfield_spaces'><strong>Required Spaces</strong></label>";
        $out .= "<select id='Inputfield_spaces' onchange='calculate()' class=' required' name='spaces' required>";
          $out .= "<option value=''></option>";
           $single_spaces = $cruise->cruise_single;
          if ($single_spaces == 0) {
            $range = range(2,$cruise->cruise_spaces,2);
           } else {
          $range = range(1,$cruise->cruise_spaces);}
          foreach ($range as $space) {$out .= "<option value='$space'>$space</option>";}
        $out .= "</select>";
    $out .= "</div>";    

     $out .= "<div class='medium-6 columns'>";
       $out .= "<label for='additional'><strong>Names of Additional Guests</strong></label>";
       $out .= "<textarea name='additional' maxlength='8192' rows='5'></textarea>";
     $out .= "</div>";
 $out .= "</div>";
 $out .= "</div>";
 $out .= "<div class='cart-box'>";

       $out .= "<h2>Payment Information</h2>";

 $out .= "<div class='row'>";
     $out .= "<div class='medium-6 columns'>";
     $out .= "<strong><span id='number_guests'></span> Guest(s)</strong> x &pound;$cruise->cruise_price<br>";
     $out .= "<strong>Total Payment</strong> : <span id='total_price'>£00.00</span><br>";
     $out .= "<strong>Balance Remaining</strong> : <span id='balance_price'>£00.00</span><br>";
     $out .= "<div class='paytoday'><strong>Amount Payable today</strong> : <span id='amount_price'>£00.00</span></div><br>";
      
    $out .= "</div> ";
     $out .= "<div class='medium-6 columns'>";
     if ($paymentrequired == 100) {
   $out .= "<div class='callout warning'><p>This cruises is within the next 12 weeks the full cruise should be collected.</p></div>";
}else{
 $out .= "<div class='callout success'><p>To secure your booking a 20&#37; Deposit today.</p></div>";
}
     $out .= "</div>";    
 $out .= "</div>"; 

 $out .= "<div class='row'>";
     $out .= "<div class='medium-12 columns'>";
    
     $out .= "<input id='price' hidden class=' required InputfieldMaxWidth' readonly name='price' maxlength='512' value='$cruise->cruise_price'>";
     $out .= "</div>";

 $out .= "</div>";

 $out .= "<div class='row'>";
     $out .= "<div class='medium-6 columns'>";
       $out .= "<button type='submit' name='submit' value='submit' class='button success'>Add booking</button>";
     $out .= "</div>";

        $out .= "<div class='medium-6 columns'>";
       $out .= "<label for='amount'><strong>Amount Paid</strong></label>";
       $out .= "<input name='amount' type='text' maxlength='20' value='$amount'>";
     $out .= "</div>";
 $out .= "</div>";
 $out .= "</div>";
 $out .= "</form>";
 $out .= "<div class='callout alert selectspaces'>";
   $out .= "<h5>Please select spaces required before continuing</h5>";
 $out .= "</div>";
  echo $out;

}

?>


