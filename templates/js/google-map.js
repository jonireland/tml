


        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 7,
                    scrollwheel: false,
                    zoomControl: false,
  mapTypeControl: false,
  scaleControl: false,
  streetViewControl: false,

                    // The latitude and longitude to center the map (always required),-,
                    center: new google.maps.LatLng(56.982262, -5.690998), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [
                    {"featureType":"all","elementType":"labels.text","stylers":[{"weight":"0.01"}]},
                    {"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"}]},
                    {"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#000000"}]},
                    {"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},
                    {"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"}]},
                    {"featureType":"administrative","elementType":"labels.text","stylers":[{"color":"#000000"}]},
                    {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#000000"}]},
                    
                    {"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"weight":"1.23"}]},
                    {"featureType":"administrative.country","elementType":"labels.text","stylers":[{"color":"#000000"},{"weight":"0.01"}]},
                    
                    {"featureType":"administrative.province","elementType":"geometry","stylers":[{"weight":"0.01"},{"color":"#000000"}]},
                    {"featureType":"administrative.province","elementType":"labels","stylers":[{"weight":"0.01"},{"visibility":"on"},{"color":"#000000"}]},
                    {"featureType":"administrative.province","elementType":"labels.text","stylers":[{"weight":"0.01"},{"color":"#000000"}]},
                    {"featureType":"administrative.province","elementType":"labels.text.fill","stylers":[{"weight":"0.01"},{"color":"#000000"}]},
                    {"featureType":"administrative.province","elementType":"labels.text.stroke","stylers":[{"weight":"0.01"},{"color":"#000000"}]},
                    
                    {"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"color":"#333333"},{"weight":"0.01"},{"visibility":"on"}]},
                   
                    {"featureType":"landscape","elementType":"all","stylers":[{"color":"#000000"}]},
                    
                    {"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#f4eee2"}]},
                    
                    {"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#039eba"}]},
                    {"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"color":"#d3d5e4"}]},
                    
                    {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},
                    {"featureType":"road","elementType":"geometry","stylers":[{"color":"#c5c0ba"},{"visibility":"off"}]},
                    
                    {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},

                    
                    {"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
                    
                    {"featureType":"water","elementType":"all","stylers":[{"color":"#136b7a"},{"visibility":"on"}]},
                    {"featureType":"water","elementType":"geometry","stylers":[{"color":"#ff0000"}]},
                    {"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#e9e9f3"}]},
                    {"featureType":"water","elementType":"labels","stylers":[{"weight":"0.01"}]},
                    {"featureType":"water","elementType":"labels.text","stylers":[{"color":"#000000"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>

                var map = new google.maps.Map(document.getElementById('map'),
mapOptions);

                // Create the Google Map using our element and options defined above
            

                var rectangle = new google.maps.Rectangle({
    strokeColor: '#163072',
    strokeOpacity: 0.8,
    strokeWeight: 4,
    fillColor: '#5171a2',
    fillOpacity: 0.1,
    map: map,
    bounds: {
      north: 58.560748,
      south: 55.2643854,
      east: -3.6644418,
      west: -7.720834,
    }
  });

            }

google.maps.event.addDomListener(window, 'load', init)
google.maps.event.addDomListener(window, "resize", init)