 <section class='main-content'>
 	<main><!--| Main Body |-->         			
 		<!-- CENTRE COLUMN - MAIN -->
 		<div id="main"><?php echo $content?></div>
 	</main><!-- #main -->
 	
 	<!-- RIGHT COLUMN - SIDEBAR --> 
 	<div class="main-sidebar"><?php include_once("blog-side-bar.inc"); ?></div><!-- #sidebar -->
 </section>
 
