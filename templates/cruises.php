 <?php
if($page->cruise_header_image) {
          $small = $page->cruise_header_image->width(660);
          $medium = $page->cruise_header_image->width(1024);
          echo "<img data-interchange='[$small->url, small], [$medium->url, medium], [{$page->cruise_header_image->url}, large]' alt='{$page->cruise_header_image->description}'>"; 
        }
?>
<section class="main-content">
        <main><!--| Main Body |-->
        <div class="sub-info">
          <h1 class=""><?php echo $page->title . " " . $page->number_of_nights . "-nights";?></h1>
          <?php echo $page->cruise_subinfo;?></div>
          <?php echo $page->body;
      
      if(count($page->gallery)) {
			$gal = "<div class='cruise-images'>";
				$gal .= "<div class='row'>";
					$gal .= "<div class='large-8 columns'>";
            $gal .= "<div class='row'>";
               $arr = array();
            $image_items = $page->gallery;
            $image_count = 0;
              foreach ($image_items as $image_item) {
                $thumbnail = $image_item->size(220, 170);
                if ($image_count < 3) {
                $gal .= "<div class='small-4 columns thumbnails'>";
               $gal .= "<img class='thumbnail' src='{$thumbnail->url}' alt='$image_item->description'>";
               $gal .= "</div>";
                $image_count++;
              };
                $arr[] = array('src' => $image_item->url,
                'title' => $image_item->description,
        );}
            
					$gal .= "</div>";
        $gal .= "</div>";
					$gal .= "<div class='large-4 columns'>";
            $gal .= "<div class='hide-for-large gallery-buttons'>";
						$gal .= "<p><a class='gallery'>View Itinerary Gallery</a></p>";
          $gal .= "</div>";
          $gal .= "<div class='show-for-large gallery buttons'>";
            $gal .= "<h4>Itinerary Gallery</h4>";
            $gal .= "<p><a class='gallery'>Click Here</a></p>";
          $gal .= "</div>";
					$gal .= "</div>";
				$gal .= "</div>";
			$gal .= "</div>"; 
      echo $gal;
      }//Close Gallery
      ?>
		<ul class="tabs" data-tabs>
      				<?php /*<li class="tabs-title is-active"><a href="#stop-overs" aria-selected="true">Features</a></li>*/?>
   <?php if ($page->cruise_map) { echo '<li class="tabs-title is-active"><a href="#cruise-map">Cruise Area</a></li>';}?>
		 <?php if ($page->cruise_area) { echo '<li class="tabs-title"><a href="#cruise-area">Cruise</a></li>';}
    $children = $page->children('sort=cruise_start, cruise_year=2017' );
          if ($children->count() >= 1) { echo "<li class='tabs-title'><a href='#dates2017'>2017 Dates</a></li>";}?>
             <?php $children = $page->children('sort=cruise_start, cruise_year=2018' );
          if ($children->count() >= 1) { echo "<li class='tabs-title'><a href='#dates2018'>2018 Dates</a></li>";}?>
        <?php $children = $page->children('sort=cruise_start, cruise_year=2019' );
          if ($children->count() >= 1) { echo "<li class='tabs-title'><a href='#dates2019'>2019 Dates</a></li>";}?>
		</ul>
			
		<div class="tabs-content">
		
		 <?php /*<div class="tabs-panel is-active" id="stop-overs">
     <h2>Features within the cruise itinerary</h2>*/?>
     <?php /*echo $page->cruise_stopover_text; */?>
          <?php /*<ul>
             foreach($page->stopovers as $stopover) {
             //echo "<li>$stopover</li>";
            }
        </ul>
      </div> */ ?>
		
		
		   <?php if ($page->cruise_area) { 
        echo '<div class="tabs-panel " id="cruise-area">';
		    echo '<h2>Cruise</h2>';
		    echo $page->cruise_area;
		    echo '</div>';}?>
        <?php
          $children = $page->children('sort=cruise_start, cruise_year=2017' );
              if ($children->count() >= 1) {
      $out = "<div class='tabs-panel availability' id='dates2017'>";
      $out .= "<div class='header'><p>{$page->title}</p></div>";
      $out .= "<table class='scroll'>"; 
      $out .= "<tr><th>Date</th><th>Nights</th><th>Spaces</th><th>Price pp</th><th>Vessel</th><th class='text-center'>Book</th></tr>";
      $days = $page->number_of_nights;

            foreach($children as $child) {
              $price = number_format($child->cruise_price,2);
              $out .= "<tr class='year{$child->cruise_year}'>";
                $out .= "<td style='min-width:110px;'>{$child->cruise_start}</td>";
                $out .= "<td style='min-width:60px;'>{$page->number_of_nights}</td>";
                                 if ($child->cruise_spaces < 1) {
                    $out.= "<td style='min-width:60px;'>0";
                   }else{
                   $out.= "<td style='min-width:60px;'>{$child->cruise_spaces}";}
                  if ($child->cruise_spaces >=1 && $child->cruise_single >=1) {
                      $out.= " <span data-tooltip aria-haspopup='true' class='has-tip left' data-disable-hover='false' tabindex='4' title='Single Cabin(s) available.'><i class='fa fa-male'></i></span>";
                   }

                $out .= "</td>";
                $out .= "<td style='min-width:60px;'>&pound;{$price}</td>";
                $out .= "<td style='min-width:60px;'>{$child->vessels->title}</td>";
                 if ($child->cruise_spaces < 1) {
                    $out.= "<td style='min-width:175px;' class='fullybooked'x'><span>Fully Booked</span></td>";
                   }else{
                   $out.= "<td style='min-width:175px;'><a class='book-now' href='{$child->url}'>Book Now</a></td>";}
                   
                $out .= "</tr>";

               
            }
            $out .= "</table>";
            if ($page->cruise_depart) {
            $out .= "<div class='depart'>{$page->cruise_depart}</div>";
          }
          $out .= "</div>";
            echo $out;
        }
         ?>
         
          <?php
          $children = $page->children('sort=cruise_start, cruise_year=2018' );
              if ($children->count() >= 1) {
      $out = "<div class='tabs-panel availability' id='dates2018'>";
      $out .= "<div class='header'><p>{$page->title}</p></div>";
      $out .= "<table class='scroll'>"; 
      $out .= "<tr><th>Date</th><th>Nights</th><th>Spaces</th><th>Price pp</th><th>Vessel</th><th class='text-center'>Book</th></tr>";
      $days = $page->number_of_nights;

            foreach($children as $child) {
              $price = number_format($child->cruise_price,2);
              $out .= "<tr class='year{$child->cruise_year}'>";
                $out .= "<td style='min-width:110px;'>{$child->cruise_start}</td>";
                $out .= "<td style='min-width:60px;'>{$page->number_of_nights}</td>";
                $out .= "<td style='min-width:60px;'>{$child->cruise_spaces}";
                  if ($child->cruise_spaces >=1 && $child->cruise_single >=1) {
                      $out.= " <span data-tooltip aria-haspopup='true' class='has-tip left' data-disable-hover='false' tabindex='4' title='Single Cabin(s) available.'><i class='fa fa-male'></i></span>";
                   }

                $out .= "</td>";
                $out .= "<td style='min-width:60px;'>&pound;{$price}</td>";
                $out .= "<td style='min-width:60px;'>{$child->vessels->title}</td>";
                 if ($child->cruise_spaces < 1) {
                    $out.= "<td style='min-width:175px;' class='fullybooked'x'><span>Fully Booked</span></td>";
                   }else{
                   $out.= "<td style='min-width:175px;'><a class='book-now' href='{$child->url}'>Book Now</a></td>";}
                   
                $out .= "</tr>";

               
            }
            $out .= "</table>";
            if ($page->cruise_depart) {
            $out .= "<div class='depart'>{$page->cruise_depart}</div>";
          }
          $out .= "</div>";
            echo $out;
        }
         ?>
                 <?php
          $children = $page->children('sort=cruise_start, cruise_year=2019' );
              if ($children->count() >= 1) {
      $out = "<div class='tabs-panel availability' id='dates2019'>";
      $out .= "<div class='header'><p>{$page->title}</p></div>";
      $out .= "<table class='scroll'>"; 
      $out .= "<tr><th>Date</th><th>Nights</th><th>Spaces</th><th>Price pp</th><th>Vessel</th><th class='text-center'>Book</th></tr>";
      $days = $page->number_of_nights;

            foreach($children as $child) {
              $price = number_format($child->cruise_price,2);
              $out .= "<tr class='year{$child->cruise_year}'>";
                $out .= "<td style='min-width:110px;'>{$child->cruise_start}</td>";
                $out .= "<td style='min-width:60px;'>{$page->number_of_nights}</td>";
                                 if ($child->cruise_spaces < 1) {
                    $out.= "<td style='min-width:60px;'>0";
                   }else{
                   $out.= "<td style='min-width:60px;'>{$child->cruise_spaces}";}
                  if ($child->cruise_spaces >=1 && $child->cruise_single >=1) {
                      $out.= " <span data-tooltip aria-haspopup='true' class='has-tip left' data-disable-hover='false' tabindex='4' title='Single Cabin(s) available.'><i class='fa fa-male'></i></span>";
                   }

                $out .= "</td>";
                $out .= "<td style='min-width:60px;'>&pound;{$price}</td>";
                $out .= "<td style='min-width:60px;'>{$child->vessels->title}</td>";
                 if ($child->cruise_spaces < 1) {
                    $out.= "<td style='min-width:175px;' class='fullybooked'x'><span>Fully Booked</span></td>";
                   }else{
                   $out.= "<td style='min-width:175px;'><a class='book-now' href='{$child->url}'>Book Now</a></td>";}
                   
                $out .= "</tr>";

               
            }
            $out .= "</table>";
            if ($page->cruise_depart) {
            $out .= "<div class='depart'>{$page->cruise_depart}</div>";
          }
          $out .= "</div>";
            echo $out;
        }
         ?>
         
      <?php if ($page->cruise_map) { 
        echo '<div class="tabs-panel is-active" id="cruise-map">';
        echo '<h2>Cruise Area</h2>';
        echo "<img src='{$page->cruise_map->url}'>"; 
        echo '</div>';}?>

    </div>
		</div>
 </main>
    
        <div class="main-sidebar"><!--| Sidebar Info |-->
            <ul class="menu vertical child-sub">
                    <?php
                    $children = $page->siblings->not("template=cruises");
                    foreach ($children as $child) {
                         if($child->id == $page->id) {
               echo "<li class='active'>";
           } else {
               echo "<li>";
           }
                        echo "<a href=\"{$child->url}\">{$child->title}</a></li>";}
                    ?>
                    </ul>
        	<ul class="menu vertical cruise-sidebar">
        	<li class="parent"><a href="../">Our Cruises</a></li>
        	<?php
			$cruises = wire('pages')->find("template=cruises, sort=sort")->not("exclude_from_cruise_list=1");
			foreach ($cruises as $cruise) {
				 if($cruise->id == $page->id) {
       echo "<li class='active'>";
   } else {
       echo "<li>";
   }
				echo "<a href=\"{$cruise->url}\">{$cruise->title} {$cruise->number_of_nights}-nights</a></li>";}
			?>
			</ul>
       <?php
              // render widgets
              $widgets = $page->widget; 
              foreach($widgets as $widget) {
                echo $widget->render();
              } 
              ?>    

      </div>
      </section> 
<script>
$(document).ready(function() {
  var rows = $('table.scroll tr');

  $('.gallery').magnificPopup({
    items: <?php echo json_encode($arr);?>,
    gallery: {
      enabled: true
    },
    type: 'image' // this is a default type
});
});
</script>


