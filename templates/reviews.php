<?php include("./includes/header.inc");  
           $startyear = "1 January 2014";
            $endyear = "30 December 2030"; 
          ?>

          <div class="row">
        <?php
            $children = wire('pages')->find("template=add-reviews, review_date>$startyear, review_date<=$endyear, sort=-review_date");
            echo "<div class=\"reviews\">\n";
            foreach($children as $child) {
                echo "<article>\n";
                echo "<p class=\"review-date\"><i class=\"fa fa-anchor\"></i> {$child->review_date}</p>\n";
                echo "<h3><a href=\"{$child->url}\">{$child->review_cruise->title}</a></h3>\n";
                echo "<p class=\"review-guest\">{$child->review_name}</p>\n";
                echo "{$child->review_excerpt}\n";
                echo "<p><a href=\"{$child->url}\" class=\"secondary button\">Read Full Review</a></p>\n";
                echo "<hr>";
                echo "</article>\n";
            }
            echo "</div>\n";
            ?>
        </main>
        <?php include("./includes/sidebar.inc"); ?>
      </section>