<?php

if ($page->id == "1141") {$form = $forms->render('add-reviews');}

if ($page->id == "2239") {$form = $forms->render('contact_form');}

if ($page->id == "2211") {$form = $forms->render('request-a-brochure');} 

if ($page->id == "2252") {$form = $forms->render('private-charter-form');} ?>



<!DOCTYPE html>

<html lang="en">

  <head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if($page->template == 'cruise-dates') { ?>
    <meta name="robots" content="noindex">
    <?php } ?>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo $config->urls->templates?>css/app.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $config->urls->templates?>icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $config->urls->templates?>icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $config->urls->templates?>icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $config->urls->templates?>icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $config->urls->templates?>icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $config->urls->templates?>icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $config->urls->templates?>icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $config->urls->templates?>icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $config->urls->templates?>icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $config->urls->templates?>icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $config->urls->templates?>icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $config->urls->templates?>icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $config->urls->templates?>icons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $config->urls->templates?>icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $config->urls->templates?>icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script src="<?php echo $config->urls->templates?>bower_components/jquery/dist/jquery.js"></script>

    <?php 
    if ($page->id == 2990) {
        echo '<link rel="stylesheet" href="'.$config->urls->templates.'css/lity.min.css">';
        echo '<script src="'.$config->urls->templates.'js/lity.min.js"></script>';
    }

    if ($page->id == 2239 || $page->id == 1141 || $page->id == 2211 || $page->id == 2252) {

      echo $form->styles;

      echo $form->scripts;

    }?>

    

    <script src="https://use.typekit.net/qcu2yhz.js"></script>

    <script>try{Typekit.load({ async: true });}catch(e){}</script>

  </head>

  <body>

  <?php if($page->editable()) echo "
<div class='site-admin columns large-12 right' data-sticky-container>
<div class='sticky' data-sticky data-margin-top='0'>
  <div class='admin'><img src='{$config->urls->templates}img/clearbox-designs.png'><a href='{$config->urls->root}site-manager'><i class='fa fa-tachometer'></i> Dashboard</a><div class='float-right'>
  <a href='$page->editURL'><i class='fa fa-pencil'></i> Edit Page</a> // 
  <a href='{$config->urls->root}site-manager/add-booking-1/'><i class='fa fa-anchor'></i> Add Booking</a> // 
  <a href='{$config->urls->root}site-manager/page/add/?parent_id=1351'><i class='fa fa-newspaper-o'></i> Add News Article</a> //
  <a href='{$config->urls->root}site-manager/login/logout/'><i class='fa fa-sign-in'></i> Logout</a>
</div></div></div></div>"; ?>
  <div class="small-book show-for-small-only">
                <a class="small-cal-button float-right" href="<?php echo $config->urls->root?>calendar/">Book Now <i class="fa fa-calendar"></i></a>
    
  </div>
  <div class="main-header hide-for-small">

  <div class="row">

      

      <div class="small-12 medium-5 columns">

        <a href="<?php echo $config->urls->root?>"><img class="logo" src="<?php echo $config->urls->templates?>img/majestic-lines-logo.svg"></a>

      </div>

        

      <div class="medium-7 columns show-for-medium">

        <div class="row">

          <div class="large-12 columns">
   
                  <a href="<?php echo $config->urls->root?>contact/" class="button float-right">Contact Us</a> 

                  <div class="tel float-right">+44(0) 1369 707 951</div>

                  <a class="icons float-right" href="https://www.instagram.com/themajesticline/" target="_blank"><span class="fa-stack fa-lg">

                    <i class="fa fa-circle fa-stack-2x"></i>

                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>

                </span></a>

                <a class="icons float-right" href="https://www.facebook.com/TheMajesticLine/" target="_blank"><span class="fa-stack fa-lg">

                    <i class="fa fa-circle fa-stack-2x"></i>

                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>

                </span></a>

                <a class="icons float-right" href="https://twitter.com/TheMajesticLine" target="_blank"><span class="fa-stack fa-lg">

                    <i class="fa fa-circle fa-stack-2x"></i>

                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>

                </span></a>

              

          </div>



          <div class="large-12 columns">

            <a class="cruise-cal-button float-right" href="<?php echo $config->urls->root?>calendar/"><span>View Our</span> Cruise Calendar <i class="fa fa-calendar"></i></a>

          </div>

        </div>

      </div>

    </div>

  </div>





<div class="title-bar row" data-responsive-toggle="main-menu" data-hide-for="medium">

<div class="row">

  <div class="medium-12 columns">

  <button class="menu-icon" type="button" data-toggle>

  <div class="title-bar-title">Menu</div></button> <a class="float-right" href="https://www.facebook.com/TheMajesticLine/" target="_blank"><span class="fa-stack fa-lg">

                    <i class="fa fa-circle fa-stack-2x"></i>

                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>

                </span></a>

                <a class="float-right" href="https://twitter.com/TheMajesticLine" target="_blank"><span class="fa-stack fa-lg">

                    <i class="fa fa-circle fa-stack-2x"></i>

                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>

                </span></a>

                <a class="float-right" href="<?php echo $config->urls->root?>contact/" target="_blank"><span class="fa-stack fa-lg">

                    <i class="fa fa-circle fa-stack-2x"></i>

                    <i class="fa fa-phone fa-stack-1x fa-inverse"></i>

                </span></a>

</div></div>

</div>



<div class="top-bar" id="main-menu">

<div class="row">

  <div class="medium-12 columns">

  <div class="top-bar-left">

    <ul class="menu-nav small">

       <?php

            $root = $pages->get("/");

            $children = $root->children("top_nav=0");

            $children->prepend($root);

            foreach($children as $child) {

              if (strlen(trim($child->link_text)) > 0){

                 $link = $child->link_text;}

                else 

                  {$link = $child->title;}

              $current = $child->id == $page->rootParent->id ? ' class="active"' : '';

                echo "<li {$current}><a href='{$child->url}'>{$link}</a></li>";

            }

            ?>
  
    </ul>

  </div>

</div></div>

</div>