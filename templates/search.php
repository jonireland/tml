<?php
echo $session->full_name;
/**
 * Search template
 *
 */

$selector ="";
$out ='';

$summary = array(
	'title' => '',
	'cruise_month' => '',
	'cruise_year' => '',
	);

if($input->get->title == "pc") {
    $linkyear = $input->get->cruise_year;
    $url = $config->urls->root . "private-charter/#".$linkyear;
  header('Location: '.$url);
  }

if($input->get->title) {
	$title = $pages->get($input->get->title);
	$value = $sanitizer->selectorValue($input->get->title);
	$input->whitelist('title', $value);
   
	if($title->id) {
		$selector .= "parent=$title, ";
		$summary["cruise_name"] = $title->title;
	} 

}
if ($input->get->cruise_month) {
	$value = $sanitizer->selectorValue($input->get->cruise_month);
	$selector .= "cruise_month=$value, "; 
	$summary["cruise_month"] = $sanitizer->entities($value); 
	$input->whitelist('cruise_month', $value); 
}

if ($input->get->cruise_year) {
	$value = $sanitizer->selectorValue($input->get->cruise_year);
	$selector .= "cruise_year=$value, "; 
	$summary["cruise_year"] = $sanitizer->entities($value); 
	$input->whitelist('cruise_year', $value); 
}
if ($input->get->vessels) {
  $value = $sanitizer->selectorValue($input->get->vessels);
  $selector .= "vessels=$value, "; 
  $summary["vessels"] = $sanitizer->entities($value); 
  $input->whitelist('vessels', $value); 
}

if (empty($selector))
{
  $out .= "";
}else{
  $cruises = $pages->find("$selector, sort=cruise_start"); //gets the repeater field
  if (count($cruises)) {
    $years = array();
    
    $out ="";
    // find the array of years for all events
    foreach ($cruises as $cruise) {
        $years[]= date("Y", $cruise->getUnformatted("cruise_start"));
        // add properties event_year and event_month to the $event object
        $cruise->cruise_year = date("Y", $cruise->getUnformatted("cruise_start"));
        $cruise->cruise_month = date("F", $cruise->getUnformatted("cruise_start"));
    }

    $years = array_unique($years);
    asort($years);

    // for testing
    // print_r($years);
    // print_r($events);

    foreach($years as $key => $year) {

        // Output the year
        // $out .="<h2>{$year}</h2>";

        // find the array of events this year and put into new array $year_events
        $year_cruises = $cruises->find("cruise_year=$year");
          // print_r($year_events);


        // loop through the events for this year and add the months to the array
        $months = array();
        foreach ($year_cruises as $year_cruise) {
            $months[]= date("F", $year_cruise->getUnformatted("cruise_start"));
        }
        $months = array_unique($months);

          // print_r($months);

        // loop through the months and find events for the month
        foreach($months as $key => $month) {
            
            // Output the month as a number
            $out .="<h4 class='leaving'>Cruises Leaving in {$month} {$year}</h4>";
            $out.= "<table class='scroll'>";
            $out.= "<thead><tr><th>Date</th><th>Cruise</th><th>No. of Nights</th><th>Spaces Left</th><th>Vessel</th><th>Price</th><th>Book</th></tr></thead>";
            // filter only the events for this month, from the $year_events array.
            $month_cruises = $year_cruises->find("cruise_month=$month");
              // print_r($month_events);

                foreach($month_cruises as $e) {
                  $cruise_date = date("d M Y", $e->getUnformatted("cruise_start"));
                  $price = number_format($e->cruise_price,2);
                   $out.= "<tr>";
                   $out.= "<td style='min-width:100px;'>{$cruise_date}</td>";
                   if ($e->parentID == 1877 || $e->parentID == 1809 ||$e->parentID == 1447||$e->parentID == 4622) {
                    $out.= "<td style='min-width:200px; width:40%;'><a href='{$config->urls->root}private-charter/'>{$e->parent->title}</a></td>";
                   }
                    else{
                      $out.= "<td style='min-width:200px; width:40%;'><a href='{$e->parent->url}'>{$e->parent->title}</a></td>";

                    }
                   
                   $out.= "<td style='min-width:60px;'>{$e->parent->number_of_nights}</td>";
                    
                   if(strtotime($e->cruise_start) < time() || $e->cruise_spaces < 1) {
                 $out.= "<td style='min-width:60px;'>0";
                }else { 
                   $out.= "<td style='min-width:60px;'>{$e->cruise_spaces}";
                   if ($e->cruise_spaces >=1 && $e->cruise_single >=1) {
                      $out.= " <span data-tooltip aria-haspopup='true' class='has-tip left' data-disable-hover='false' tabindex='4' title='Single Cabin(s) available.'><i class='fa fa-male'></i></span>";
                   }
                 }
                 
                   $out.= "</td>";
                   $out.= "<td >{$e->vessels->title}</td>";
                    if(strtotime($e->cruise_start) < time() || $e->cruise_spaces < 1) {
                 $out.= "<td style='min-width:60px;'>---";
                }else {
                   $out.= "<td style='min-width:60px;'>&pound;{$price}</td>";}
                  if ($e->cruise_spaces < 1 || strtotime($e->cruise_start) < time()){
                    $out.= "<td class='fullybooked' style='min-width:155px;'>Fully Booked</td>";
                   }else{
                   $out.= "<td class='booknow' style='min-width:155px;'><a href='{$e->url}'>Book Now</a>";
                   if ($e->cruise_spaces >=11 && $e->vessels->id == 1041 || $e->cruise_spaces >=11 && $e->vessels->id == 1042 || $e->cruise_spaces >=12 && $e->vessels->id == 1039 ) {
                  $out.= " <span data-tooltip aria-haspopup='true' class='has-tip left red' data-disable-hover='false' tabindex='4' title='This Cruise is also available for Private Charter.''><i class='fa fa-ship'></i></span>";
                   }
                  $out.= "</td>";}
                   $out.= "</tr>";
                } // end foreach events for this month

            $out .="</table>";

        } // end foreach months
    
    } // end foreach years
}else{
      $out = "<div class='results'>";
      $out .= "Sorry, there are no results for the cruise name and month combination you selected. Please try another.";
      $out .= "<p>If you cannot find what you are looking why not contact us.</p>";
      $out .= "</div>";
    }

}

?>
<div class="search-page">
<section class="main-content ">
  <main><!--| Main Body |-->
          <?php echo $page->body;?>
  </main>
        <div class="main-sidebar"><!--| Sidebar Info |-->
            <?php echo $page->sidebar_content;?>
        </div>
</section>
<div class="row">
  <div class="medium-12 columns">
          <form id='search_form' action='<?php echo $config->urls->root?>calendar/' method='get'>
          <div class="row">
            <div class="columns">
              <h3 class="dark">Check Availability</h3>
              <p>To view the listings for a particular cruise, and/or for a particular month, please select from the menus below.</p>
            </div>
          </div>
          <div class="row">
              <div class="medium-12 large-4 columns">
                <label for='cruise_name'>Cruise name
                  <select id='cruise_name' name='title'>
                    <option value=''>Any</option><?php
                    foreach($pages->find("template=cruises, sort=number_of_nights, sort=title")->not("exclude_from_cruise_list=1") as $range){
                      $selected = $range->id == $input->whitelist->title ? " selected='selected'" : '';
                      echo "<option$selected value='$range->id'>{$range->title} {$range->number_of_nights}-night</option>";
                      }?>
                      <option value='pc'>Private Cruise Charters</option>
                  </select>
                </label>
              </div>

                                          <div class="medium-4 large-2 columns">
                <label for='vessels'>Vessel
                  <select name="vessels" id="vessels" size="1">
                    <option value=''>Any</option>
                  <?php 
                    foreach(array('1039', '1041','1042') as $range) {
                      $selected = $range == $input->whitelist->vessels ? " selected='selected'" : '';
                      if ($range == 1039) {
                         echo "<option$selected value='$range'>Glen Etive</option>";
                      } elseif ($range == 1041) {
                        echo "<option$selected value='$range'>Glen Massan</option>";
                      } elseif ($range == 1042) {
                        echo "<option$selected value='$range'>Glen Tarsan</option>";
                      }
                      
                      }?>
                  </select>
                </label>
              </div>
              
              <div class="medium-3 large-2 columns">
                <label for='month'>Month
                  <select name="cruise_month" id="month" size="1">
                      <option value="">Any</option><?php 
                      foreach(array('04', '05', '06', '07', '08', '09', '10') as $range) {
                        $monthName = date('F', mktime(0, 0, 0, $range, 10));
                        $selected = $range == $input->whitelist->cruise_month ? " selected='selected'" : '';
                        echo "<option$selected value='$range'>$monthName</option>";
                        }?>
                  </select>
                </label>
              </div>

              <div class="medium-3 large-2 columns">
                <label for='year'>Year
                  <select name="cruise_year" id="year" size="1"><?php 
                    foreach(array('2018', '2019') as $range) {
                      $selected = $range == $input->whitelist->cruise_year ? " selected='selected'" : '';
                      echo "<option$selected value='$range'>$range</option>";
                      }?>
                  </select>
                </label>
              </div>
            <div class="medium-2 columns">

                <button class="button secondary" type='submit' id='search_submit' name='submit' value="Search">Go <i class='fa fa-arrow-circle-right fa-lg'></i></button>
            </div>
            </div>
      </form>
 
     <?php 
     echo $out;?>
</div>
</div>
</div>