<section class="main-content">
        <main><!--| Main Body |-->

<?php


$out = "";
$form = $session->order;
if (isset($form)){

$title = $session->order['parentcruisename'];
$spaces = $session->order['spaces'];
$instid = 307069;
$date = $session->order['date'];
$email = $session->order['email'];
$tel = $session->order['tel'];
$mobile = $session->order['mobile'];
$nights = $session->order['nights'];
$address = $session->order['address_1'];
$address2 = $session->order['address_2'];
$city = $session->order['city'];
$country = $session->order['country'];
$postcode = $session->order['postcode'];
$nights = $session->order['nights'];
$price = $session->order['book_singleprice'];
$total = $session->order['total'];
$fullname = $session->order['fullname'];
$cartid = $session->order['cartid'];
$amount = number_format((float)$session->order['payment'], 2, '', '');
$pricep = number_format($price,2,'.', '');
$totalp = number_format($total,2,'.', '');
$amountp = number_format($session->order['payment'],2,'.', '');
$out .= "<div class='order-summary'>";
  $out .= "<h1>Booking Summary {$cartid}</h1>";
  $out .= "<strong>Full Name : </strong>{$fullname}</br>";
  $out .= "<strong>Email : </strong> {$email}</br>";
  $out .= "<strong>Tel No : </strong> {$tel}</br><br>";
  $out .= "<strong>Address : </strong>{$address}, {$address2} {$city}, {$country}, {$postcode}</br>";
  $out .= "<img class='float-right' src='{$config->urls->templates}img/tta_logo.jpg'>";
  $out .= "<strong>Cruise : </strong>{$title} :: {$nights}-nights</br>";
  $out .= "<strong>Date : </strong>{$date}</br>";
  $out .= "<strong>No of Guests : </strong>{$spaces} x &pound;{$pricep} per person </br>";
  $out .= "<strong>Total :</strong> &pound;{$totalp}</br>";
   $out .= "<p class='amount'><strong>Amount due:</strong> &pound;{$amountp}</p>";
  $out .= "<form action='https://secure.worldpay.com/wcc/purchase' method=POST>";
// This next line contains a mandatory parameter. Put your Installation ID inside the quotes after value
$out .= "<input type='hidden' name='instId' value='{$instid}'>";
//Another mandatory parameter. Put your own reference identifier for the item purchased inside the quotes after value
$out .= "<input type='hidden' name='cartId' value='{$cartid}'>";
//Another mandatory parameter. Put the total cost of the item inside the quotes after value
$out .= "<input type='hidden' name='amount' value='{$amountp}'>";
//Another mandatory parameter. Put the code for the purchase currency inside the quotes after value

$key = "GlenEtiveNew2016Glen@:".$instid.":".$amountp.":GBP:".$cartid;
$keym = md5($key);
$out .= "<input type='hidden' name='signature' value='{$keym}'>";
$out .= "<input type='hidden' name='currency' value='GBP'>";
$out .= "<input type='hidden' name='testMode' value='0'>";
$out .= "<input type='hidden' name='desc' value='{$title} :: {$nights}-nights'>";
$out .= "<input type='hidden' name='name' value='{$fullname}'>";
$out .= "<input type='hidden' name='email' value='{$email}'>";
$out .= "<input type='hidden' name='address1' value='{$address}'>";
$out .= "<input type='hidden' name='address2' value='{$address2}'>";
$out .= "<input type='hidden' name='town' value='{$city}'>";
$out .= "<input type='hidden' name='postcode' value='{$postcode}'>";
$out .= "<input type='hidden' name='country' value='GB'>";
$out .= "<input type='hidden' name='tel' value='{$tel}'>";
$out .= "<button class='button' type='submit'><i class='fa fa-shopping-cart'></i> Proceed to Online Payment</button>";
$out .= "</form>";
  $out .= "</div>";
  $i = 2;
if ($i == 2) {
 $p = new Page(); // create new page object
$p->template = 'worldpay-bookings'; // set template
$p->parent = wire('pages')->get('/worldpay/'); // set the parent 
$p->name = strtotime ("now"); // give it a name used in the url for the page
$p->title = $session->order['parentcruisename'] . " - " . $session->order['nights'] . " Nights" . " ( " . $session->order['fullname'] . " ) ";
$p->booking_name = $session->order['fullname'];
$p->booking_email = $session->order['email'];
$p->booking_address = $session->order['address_1'];
$p->booking_address1 = $session->order['address_2'];
$p->booking_city = $session->order['city'];
$p->booking_country = $session->order['country'];
$p->booking_postcode = $session->order['postcode'];
$p->booking_tel = $session->order['tel'];
$p->booking_mobile = $session->order['mobile'];
$p->booking_spaces = $session->order['spaces'];
$p->booking_title = $session->order['parentcruisename']  . " - " . $session->order['nights'] . " Nights";
$p->booking_start = $session->order['date'];
$p->booking_cruiseid = $session->order['id'];
$p->booking_cabin = $session->order['cabin'];
$p->booking_status = 4;
$p->booking_additional = $session->order['additional'];
$p->booking_price = $session->order['book_singleprice'];
$p->booking_total = $session->order['total'];
$p->booking_amountpaid = $session->order['payment'];
$p->booking_single = $session->order['book_single_space'];
$p->booking_vessel = $session->order['vessel'];
$p->wp_nights = $session->order['nights'];
$p->wp_single_spaces = $session->order['single_spaces'];
$p->wp_book_sing = $session->order['book_sing'];
$p->wp_book_single = $session->order['book_single'];
$p->wp_cartid = $cartid;
$p->wp_cruiseid = $session->order['id'];
$p->save();
$i = 3;
}

 

  }else{
   $out .= "<div class='callout warning'>";
   $out .= "<h2>No Booking selected</h2>";
   $out .= "<p>You have not selected a cruise please head back to our cruises page.</p></div>";
   $out .= "<a class='expanded button secondary' href='{$config->urls->root}cruises/'>Back to our Cruises</a>";
  }
  echo $out;
?>

<div><p class='float-left'>Payment Processed via WorldPay</p>
<img class='float-right' src="http://www.worldpay.com/images/cardlogos/VISA.gif" border=0 alt="Visa Credit and Debit payments supported by Worldpay">

<img class='float-right' src="http://www.worldpay.com/images/cardlogos/visa_electron.gif" border=0 alt="Visa Electron payments supported by Worldpay">

<img class='float-right' src="http://www.worldpay.com/images/cardlogos/mastercard.gif" border=0 alt="Mastercard payments supported by Worldpay">

<img class='float-right' src="http://www.worldpay.com/images/cardlogos/maestro.gif" border=0 alt="Maestro payments supported by Worldpay">

<img class='float-right' src="http://www.worldpay.com/images/cardlogos/JCB.gif" border=0 alt="JCB payments supported by Worldpay"></div>

</main>
</section>

