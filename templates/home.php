          <div class="orbit" role="region" data-orbit>
            <ul class="orbit-container">
              <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span><i class="fa fa-angle-left fa-5x"></i></button>
              <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span><i class="fa fa-angle-right fa-5x"></i></button>
              <?php foreach($page->slide_show as $image) {
                    $small = $image->width(660);
                    $medium = $image->width(1024);
              $out = "<li class='orbit-slide'>";
              $out .= "<img data-interchange='[$small->url, small], [$medium->url, medium], [$image->url, large]' alt='$image->description'>";    
              $out .= "<div class='orbit-caption'>";
              $out .= "<section class='se-container'>";
              $out .= "<div class='angle'>";
              $out .= "<h2 class='white'>$image->caption</h2>";
              $out .= "<h2 class='gold'>$image->subcaption</h2>";
              $out .= "</div>";
              $out .= "</section>";
              $out .= "</div></li>";
              echo $out;
              }?>        
            </ul>
              <nav class="orbit-bullets">
              <?php $c=0;
              foreach($page->slide_show as $image) {

              echo "<button data-slide='$c'><span class='show-for-sr'>$image->caption - $image->subcaption</span></button>\r\n";
              $c++;

              }?>    
             </nav>
          </div>

      <section class="main-content">
        <main><!--| Main Body |-->
          <?php echo $page->body;?>
        </main>
    
        <div class="main-sidebar"><!--| Sidebar Info |-->
              <?php
              // render widgets
              $widgets = $page->widget; 
              foreach($widgets as $widget) {
                echo $widget->render();
              } 
              ?>        
        </div>
      </section> 
<?php if(count($page->sub_footer_images)) {
echo "<div class='cta-section'>\n";
echo "<div class='row' data-equalizer>\n";
     foreach($page->sub_footer_images as $image) {
      $sized = $image->size(530, 354);
    echo "<article >\r\n";
    echo "<a href='{$pages->get($image->link)->url}'><div class='bg' data-equalizer-watch>\r\n";
    echo "<img src='{$sized->url}' alt='{$image->description}'>\r\n";
    echo "<h4>$image->caption</h4>\r\n";
    echo "</div></a>\r\n";
    echo "</article>\r\n";}
    echo "</div>";
  }?>

</div>