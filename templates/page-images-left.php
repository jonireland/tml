<?php include("./includes/header.inc");
echo $page->body;?>
          </div>
            <div class="large-4 large-pull-8 columns">
                <div id="image-gallery" class="row small-up-2 medium-up-3 large-up-1">
                  <?php foreach ($page->gallery as $image) {
                    $large = $image->width(310);
                    $small = $image->size(280, 180);
                    echo "<div class='column'>";
                    echo "<a href='$image->url' title='$image->description'><img class='thumbnail' data-interchange='[$small->url, small], [$large->url, large]' alt='$image->description'></a>";
                    echo "</div>";
                  }

                  ?>

      </div>
        </div>

          </div>
        </main>
        <?php include("./includes/sidebar.inc"); ?>
      </section> 


<script>
$(document).ready(function() {
$('#image-gallery').magnificPopup({
  delegate: 'a', // child items selector, by clicking on it popup will open
  type: 'image',
  gallery : {enabled:true},
});
});
</script>
