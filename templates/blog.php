<?php 

/**
 * Blog Home template
 * Demo template file populated with MarkupBlog output and additional custom code for the Blog Home Page
 *
 */

    //CALL THE MODULE - MarkupBlog
    $blog = $modules->get("MarkupBlog"); 
    
    //subnav
    //we expect only one such page. we do it this way in this demo to accomodate different blog styles
    $categories = $pages->get('template=blog-categories');
    $subNav = $blog->renderNav($categories->title, $categories->children);

    //main content

    //number of posts to show on Blog Home Page (pagination kicks in if more posts than limit)
    $settings = $pages->get('template=blog-settings');//we get this from the settings page. In your own install you can use a more specific selector
    $limit = 6;
    $content = '';

    //Render limited number of posts on Blog Home Page
$options = array(
    'post_count' => 8,
    'post_comments' => 0,
    'post_small_image' => 2,
    'post_large_image_width' => '100',
    'post_small_image_width' => '300',
    'post_more_text' => $this->_('Read More'),
    'post_small_allowable_tags' => 'p',

);
    
          $out = "";
if (strlen(trim($page->page_header)) > 0){
          $link = $page->page_header;}
                else 
          {$link = $page->title;}
         if (isset($page->image_select->header_image)) {

         }else{
          $out .= "<div class='small-header'>";
          $small = $page->image_select->header_image->width(660);
          $medium = $page->image_select->header_image->width(1024);
          $out .= "<img data-interchange='[$small->url, small], [$medium->url, medium], [{$page->image_select->header_image->url}, large]' alt='{$page->image_select->header_image->description}'>";
        $out .= "<div class='small-holder'>";
          $out .= "<div class='row'>";
          $out .= "<div class='columns'>";
          $out .= "<h1>{$link}</h1>";
          $out .= "</div></div></div></div>";

         echo $out;
      }



/**** RENDER 5 SUMMARISED BLOG POSTS ****/
//first we call MarkupBlog
$blog = $modules->get("MarkupBlog");
//we will later pass this as a string to the selector renderPosts() to limit number of Blog Posts returned
$limit = 10;

//We render a maximum of 5 summarised Blog Posts
$content .= $blog->renderPosts("limit={$limit}", true, $options);



	
    ?>
     <section class='main-content'>
          <main><!--| Main Body |-->              
                <!-- CENTRE COLUMN - MAIN -->
              <div id="main">


                <?php echo $content?></div>
                </main><!-- #main -->
                  
              <!-- RIGHT COLUMN - SIDEBAR --> 
              <div class="main-sidebar"><?php include_once("blog-side-bar.inc"); ?></div><!-- #sidebar -->
            </section>