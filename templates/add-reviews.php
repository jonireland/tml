<div class="small-header">
    <img class="logo" src="<?php echo $config->urls->templates?>img/boat-with-ripples.jpg">
    <div class="small-holder">
        <div class="row">
            <div class="columns">
                <h1>Cruise Review</h1>
            </div>
        </div>
    </div>   
</div>
    <section class="main-content">
        <main><!--| Main Body |-->
          <div class="row">
            <section class="reviews">
                <article>
                    <p class="review-date"><i class="fa fa-anchor"></i> <?php echo $page->review_date;?></p>
                    <h3><?php echo $page->review_cruise->title;?></h3>
                    <p class="review-guest"><?php echo $page->review_name;?></p>
                    <?php echo $page->review;?>
                    <p><a href="<?php echo $page->parent->url;?>" class="secondary button">Back to Reviews</a></p>
                </article>        
            </section>
         </div>
        </main>
    
        <div class="main-sidebar"><!--| Sidebar Info |-->
            <ul class="menu vertical child-sub">
                    <?php
$p = $page;
        while($p->parentID != 1) {
            $p = $p->parent;
        }
$out ="";
if ($p->numChildren) {
  if ($p->id == $page->id) {
    $out .= "<li><a class='active' href='{$p->url}'>{$p->title}</a></li>";
  }else{
$out .= "<li><a href='{$p->url}'>{$p->title}</a></li>";}}

foreach($p->children->not("template=cruises, template=add-reviews") as $child) {
  
  // check if this child is in parents or direct parent of current page
  if($page->parents->has($child) || $child === $page || $child === $page->parent) $class = "active"; else $class = '';
     if (strlen(trim($child->link_text)) > 0){
                 $link = $child->link_text;}
                else 
                  {$link = $child->title;}
  $out .= "\n<li><a class='$class' href='{$child->url}'>{$link}</a>";
  if ($class=="active") {
  if($child->numChildren > 0){
    
    // check if this child is in parents or direct parent of current page

    $out .= "\n\t<ul class='menu vertical'>";
    
    foreach($child->children as $childchild) {
      
      if($childchild === $page || $childchild === $page->parent) $class = "active"; else $class = '';
      if (strlen(trim($childchild->link_text)) > 0){
                 $link = $childchild->link_text;}
                else 
                  {$link = $childchild->title;}
      $out .= "\n\t\t<li><a class='$class' href='{$childchild->url}'>{$link}</a></li>";
    }
    $out .= "\n\t</ul>";
  }}
  $out .= "\n</li>";
}

echo $out;?>
                    </ul>
             <?php
               // render widgets
              $widgets = $pages->get(1112)->widget; 
              foreach($widgets as $widget) {
                echo $widget->render();
              }

            


              ?>                
      </section>