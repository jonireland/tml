<?php
wire('modules')->get('MarkupAdminDataTable');



?>
<div class="row">
    <div class="large-6 columns">

        <div class="row">
          <div class='large-12 columns'>
        <div class="admin-container">
            <header class="blue"><h5><i class="fa fa-cogs"></i> Admin Panel</h5></header>
            <a href="<?php echo $config->urls->admin?>page" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-list fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Page Tree</h4>
                        <p>List of all pages for site</p>
                    </div>
                </div></div>
            </a>
            
            <a href="<?php echo $config->urls->admin?>add-booking-1/" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-anchor fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Add Cruise Booking</h4>
                        <p>Add a cruise booking</p>
                    </div>
                </div></div>
            </a>

            <a href="<?php echo $config->urls->admin?>setup/form-builder/listEntries/?id=2" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-user fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Contact Form Submissions</h4>
                        <p>List of all Contact form submissions.</p>
                    </div>
                </div></div>
            </a>
        </div>
    </div>
    </div>
</div>

        <div class="large-6 columns">
        <div class="row">
    <div class='large-12 columns'>
        
        <div class="admin-container">
            <header class="green"><h5><i class="fa fa-comment"></i> Reviews</h5></header>
            <a href="<?php echo $config->urls->admin?>page/add/?parent_id=1112&template_id=54" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-comment-o fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Add Review</h4>
                        <p>Add a cruise review page</p>
                    </div>
                </div></div>
            </a>

            <a href="<?php echo $config->urls->admin?>page/reviews-2/" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-comments-o fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>View Reviews</h4>
                        <p>View all Reviews</p>
                    </div>
                </div></div>
            </a>

            <a href="<?php echo $config->urls->admin?>page/reviews-2/" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-check-square-o fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <?php $count = $pages->count("template=add-reviews, status=unpublished");?>
                        <h4>Reviews awaiting approval (<?php echo $count;?>)</h4>
                        <p>Approve Reviews Submitted via site.</p>
                    </div>
                </div></div>
            </a>

        </div>


    </div>

    </div>
</div>

</div>

<div class="row">
    <div class="large-6 columns">

        <div class="row">
          <div class='large-12 columns'>
        <div class="admin-container">
            <header class="purple"><h5><i class="fa fa-anchor"></i> Cruises</h5></header>
            <a href="<?php echo $config->urls->admin?>page/add/?template_id=45" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-calendar-plus-o fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Add Cruise Date</h4>
                        <p>Add a cruise date to a specific cruise</p>
                    </div>
                </div></div>
            </a>
            
            <a href="<?php echo $config->urls->admin?>page/cruise-dates/" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-calendar fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>View Cruise Dates</h4>
                        <p>View and Search all cruise dates</p>
                    </div>
                </div></div>
            </a>

            <a href="<?php echo $config->urls->admin?>page/add/?parent_id=1015&template_id=43" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-ship fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Add New Cruise</h4>
                        <p>Add new parent cruise.</p>
                    </div>
                </div></div>
            </a>
        </div>
    </div>
    </div>
</div>

        <div class="large-6 columns">
        <div class="row">
    <div class='large-12 columns'>
        
        <div class="admin-container">
            <header class="orange"><h5><i class="fa fa-medkit"></i> Support</h5></header>
            <a href="mailto:jon@clearboxdesigns.co.uk" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-question-circle fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Got a question?</h4>
                        <p>Drop me a email. </p>
                    </div>
                </div></div>
            </a>

            <a href="#" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-phone fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Phone Support</h4>
                        <p>01631 570831 / 07510548294</p>
                    </div>
                </div></div>
            </a>
                <a href="#" class="admin-but">
                <div class='row'>
                <div class='small-2 columns'>
                    <div class="icon"><i class="fa fa-skype fa-4x"></i></div>
                </div>
                <div class='small-10 columns'>
                    <div class="button-content">
                        <h4>Skype</h4>
                        <p>clearboxdesigns</p>
                    </div>
                </div></div>
            </a>
        </div>


    </div>

    </div>
</div>

</div>

 <div class='row'>
    
  <div class='large-12 columns'>
        <div class="admin-container">
            <header class="red"><h5><i class="fa fa-bed"></i> Bookings</h5></header>
    
<?php
            $events = $pages->find("template=booking,limit=5, sort=-booking_orderno");
$table = $modules->get("MarkupAdminDataTable");
$table->headerRow( ["Name", "Spaces", "Cruise","Cruise Start", "Order No", "Status", "Date Booked", "" ] );
$table->setSortable(false);
$table->action(array(
    'Add Booking' => $config->urls->admin . 'add-booking-1/',
    'View All Bookings' => $config->urls->admin . 'page/cruise-bookings/',
));

foreach($events as $paget){
    $cleantitle = substr($paget->title, 0, strpos($paget->title, "-"));
  $data = array(
    // Values with a sting key are converter to a link: title => link
    $paget->booking_name => $config->urls->admin."page/edit/?id=".$paget->id,
    $paget->booking_spaces,
    $paget->booking_title,
    $paget->booking_start,
    $paget->booking_orderno,
    $paget->booking_status->title,
    date("d M Y", $paget->created),
    'View Booking' => $config->urls->admin."page/edit/?id=".$paget->id,
  );
  $table->row($data);
}

// $table->footerRow( $someArray );
echo '<div class="admin-table bookings">';                                 
echo $table->render();
echo '</div>';?>



        </div>
    </div>
    <div class='large-12 columns'>
        
        <div class="admin-container">
            <header class="yellow"><h5><i class="fa fa-rss"></i> News / Blog</h5></header>
           <?php
            $events = $pages->find("template=blog-post,limit=5, sort=-blog_date");
$table = $modules->get("MarkupAdminDataTable");
$table->headerRow( ["Headline", "Date Published",] );
$table->setSortable(false);
$table->action(array(
    'Add News Article' => $config->urls->admin . 'page/add/?parent_id=1351&template_id=136',
    'View All News' => $config->urls->admin . 'page/?open=1351',
));

foreach($events as $page){
  $data = array(
    // Values with a sting key are converter to a link: title => link
    $page->title => $config->urls->admin."page/edit/?id=".$page->id,
    $page->blog_date,
  );
  $table->row($data);
}

// $table->footerRow( $someArray );
echo '<div class="admin-table">';                                 
echo $table->render();
echo '</div>';?>

        </div>


    </div>
 </div>
 