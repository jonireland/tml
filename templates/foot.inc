<div class="reviews-section">

  <div class="row">

    <div class="medium-10 medium-centered columns">

      <?php

      $quote = $pages->find("template=add-reviews, exclude_from_review_footer=0, sort=random, limit=1")->first();

      $review_date = $quote->getUnformatted("review_date");

      $review_date = date("M Y", $review_date);

        echo "<div class='quote'>{$quote->review_excerpt}</div>\n";

        echo "<div class='author'>— {$quote->review_name}: {$review_date} - <a href='{$quote->url}'>Read Full Review</a></div>\n";

      ?>

    </div>

</div>

</div>



<footer class="main-footer">

  <div class="container">

	<div class="row">

      <div class="large-12 columns">

      	

        <ul>

          <li class="show-for-medium"><img class="boat float-left" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkAQMAAADbzgrbAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAAxJREFUeNpjYBieAAAA2AABB43sDAAAAABJRU5ErkJggg=="></li>

         <?php

            $root = $pages->get("/");

            $children = $root->children('foot_nav=1');

            foreach($children as $child) {

              if (strlen(trim($child->link_text)) > 0){

                 $link = $child->link_text;}

                else 

                  {$link = $child->title;}

              $current = $child->id == $page->rootParent->id ? ' class="active"' : '';

                echo "<li {$current}><a href='{$child->url}'>{$link}</a></li>";

            }

            ?>

          

        </ul>

      </div>

        <div class="large-12 columns">

      	The Majestic Line (Scotland) Ltd</br>

        Registered Office: Unit 3 Holy Loch Marina, Sandbank, Dunoon, PA23 8FE, Argyll</br>

        <p>Private Limited Company: SC 411538</p>

      	<p>Copyright &copy; <?php echo date("Y");?> The Majestic Line (Scotland) Ltd</p>

        <a href="#"><i class="fa fa-chevron-up"></i> Top</a>

      </div>

      </div>

    </div>

</footer>



    <script src="<?php echo $config->urls->templates?>bower_components/what-input/what-input.js"></script>

    <script src="<?php echo $config->urls->templates?>bower_components/foundation-sites/dist/foundation.js"></script>

    <script src="<?php echo $config->urls->templates?>js/app.js"></script>

    <script src="<?php echo $config->urls->templates?>js/jquery.magnific-popup.min.js"></script>

</body>

</html>

