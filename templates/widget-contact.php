  <div class="contact-info">
        	<h4>Our Address</h4>
        	<div class="address">
					<address>The Majestic Line</address>
					<address>Holy Loch Marina</address>
					<address>Sandbank</address>
					<address>Dunoon</address>
					<address>Argyll</address>
					<address>PA23 8FE</address>
				</div>
					<h4>Our Email Address</h4>
						<p class="email"><a href="mailto:info@themajesticline.co.uk?subject=Website Enquiry"><i class="fa fa-envelope-o"></i> info@themajesticline.co.uk</a></p>
					<h4>Our Telephone No.</h4>
						<p class="telephone"><i class="fa fa-phone"></i> +44 (0)1369 707 951</p>

</div>