   <section class="main-content">
        <main><!--| Main Body |-->
          <h1 class="title"><?php echo $page->title;?></h1>
          <?php echo $page->body;?>
        </main>
    
        <div class="main-sidebar"><!--| Sidebar Info |-->
                   
             <?php
              // render widgets
              $widgets = $page->widget; 
              foreach($widgets as $widget) {
                echo $widget->render();
              } 
              ?>    
        </div>
      </section> 
<div class="row medium-up-4 cruise-list" data-equalizer>
        <?php
            $children = $page->children("template=single-video, sort=title");
            
            foreach($children as $child) {

                echo "<div class='columns'>";
                echo "<a href='{$child->url}'><div class='bg' data-equalizer-watch>\r\n";
                if($child->video_thumb) 
                $sized = $child->video_thumb->size(530, 354);
                echo "<div class='image-container'><img class='img' src='{$sized->url}'></div>";
                echo "<p>{$child->title}</p>";
                echo "</div></a>\r\n";
                echo "</div>";
                $sized = "0";
            }
            ?>
               
</div>



