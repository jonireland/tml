<?php

$id = $_GET['id'];

$modules->get('ProcessPageLister');
$bookmark = array(
    'initSelector' => "booking_cruiseid=$id",
    'defaultSelector' => "template=booking",
    'defaultSort' => "",
    'columns' => array("title", "booking_name", "booking_status","booking_spaces",),
    /*'toggles' => array('noButtons'),*/
    'allowBookmarks' => false,
    'allowIncludeAll' => false,
    'viewMode' => 4,
    'editMode' => 4,
    'editOption' => 0,
);
$id = "mylister";
$url = ProcessPageLister::addSessionBookmark($id, $bookmark);
$session->redirect($url);
?>